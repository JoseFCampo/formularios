FROM python:3.8-slim
ENV PYTHONUNBUFFERED 1
RUN apt-get update
RUN mkdir /opt/oracle && cd /opt/oracle 
RUN apt-get install apt-utils -y && apt-get install wget -y && apt-get install unzip -y && apt-get install libaio1 -y 
RUN wget https://download.oracle.com/otn_software/linux/instantclient/19600/instantclient-basic-linux.x64-19.6.0.0.0dbru.zip
RUN unzip instantclient-basic-linux.x64-19.6.0.0.0dbru.zip && rm instantclient-basic-linux.x64-19.6.0.0.0dbru.zip
RUN rm -rf /var/lib/apt/lists/*
RUN sh -c "echo /instantclient_19_6 > /etc/ld.so.conf.d/oracle-instantclient.conf" && ldconfig
RUN export LD_LIBRARY_PATH=/instantclient_19_6:$LD_LIBRARY_PATH

RUN mkdir /backend
WORKDIR /backend
COPY . /backend/
RUN pip install -r requirements.txt
#RUN python manage.py migrate --settings forms.settings.prod
# CMD ["python","manage.py","runserver"]

