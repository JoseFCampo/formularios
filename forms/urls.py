# from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
# from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    # path('grappelli/', include('grappelli.urls')),
    # path('admin/', admin.site.urls),
    path('api/control/', include('apps.control.urls')),
    path('api/aglov/', include('apps.referentes.urls')),
    path('api/especies/', include('apps.especies.urls')),
    path('api/unidades/', include('apps.unidades.urls')),
    path('api/parametros/', include('apps.parametros.urls')),
    path('api/metodos/', include('apps.metodos.urls')),
    path('api/metodologias/', include('apps.metodologias.urls')),
    path('api/tematicas/', include('apps.tematicas.urls')),
    path('api/estaciones/', include('apps.estaciones.urls')),
    path('api/user/', include('apps.usuarios.urls')),
    path('api/proyectos/', include('apps.proyectos.urls')),
    path('api/qflag/', include('apps.qflag.urls')),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
