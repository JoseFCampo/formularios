from .base import *

import corsheaders
import debug_toolbar
from datetime import timedelta

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

INTERNAL_IPS = [
    '127.0.0.1',
    'localhost',
    '0.0.0.0'
]

CORS_ORIGIN_ALLOW_ALL = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'datos': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'DATOSDECAMPO',
        'PASSWORD': 'perdidosporaca',
        'HOST': '192.168.3.77',
        'PORT': '1521',
    },
    'usuarios_sci': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'usuarios_sci',
        'PASSWORD': 'investigadores',
        'HOST': '192.168.3.77',
        'PORT': '1521',
    },
    'geograficos': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'geograficos',
        'PASSWORD': 'estaciones',
        'HOST': '192.168.3.77',
        'PORT': '1521',
    },

}

INSTALLED_APPS += ['debug_toolbar',]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
