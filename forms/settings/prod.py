from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*',]

CORS_ORIGIN_ALLOW_ALL = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    'datos': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'DATOSDECAMPO',
        'PASSWORD': 'paseos',
        'HOST': '192.168.3.70',
        'PORT': '1521',
       
    },
    'usuarios_sci': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'usuarios_sci',
        'PASSWORD': 'investigadores',
        'HOST': '192.168.3.70',
        'PORT': '1521',
    },
    'geograficos': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': 'sci',
        'USER': 'geograficos',
        'PASSWORD': 'estaciones',
        'HOST': '192.168.3.70',
        'PORT': '1521',
    },

}