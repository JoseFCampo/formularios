

class DatosRouter:

    apps = {
        'referentes',
        'unidades',
        'especies',
        'parametros',
        'metodos',
        'metodologias',
        'tematicas',
        'proyectos'
    }
    geo = {'estaciones'}
    users = {'usuarios'}

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.apps:
            return 'datos'
        if model._meta.app_label in self.geo:
            return 'geograficos'
        if model._meta.app_label in self.users:
            return 'usuarios_sci'
        return None

    def db_for_write(self, model, **hints):

        if model._meta.app_label in self.apps:
            return 'datos'  # Seleccionamos la Base de Datos datos
        if model._meta.app_label in self.geo:
            return 'geograficos'
        if model._meta.app_label in self.users:
            return 'usuarios_sci'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):

        if app_label in self.apps:
            return db == 'datos'
        if app_label in self.geo:
            return 'geograficos'
        if app_label in self.users:
            return 'usuarios_sci'

        return None

    def allow_relation(self, elem1, elem2, **hints):

        if (elem1._meta.app_label in self.apps and elem2._meta.app_label in self.apps) or (elem1._meta.app_label in self.geo and elem2._meta.app_label in self.geo) or (elem1._meta.app_label in self.users and elem2._meta.app_label in self.users):
            return True
        return None
