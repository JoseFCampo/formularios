from django.contrib import admin
from .models import AgmParametros


class AgmParametrosAdmin(admin.ModelAdmin):
    list_display = ('id_parametro', 'nombre', 'codigo_letras', 'siglas')
    list_display_links = ('id_parametro', 'nombre', 'codigo_letras', 'siglas')
    search_fields = ['id_parametro', 'nombre', 'codigo_letras', 'siglas']


admin.site.register(AgmParametros, AgmParametrosAdmin)
