from apps.parametros.views import(
    agmparametros_one,
    agmparametros_create,
    agmparametros_update,
    agmparametros_all,)
from django.urls import path


urlpatterns = [
    path('u/<int:id_parametro>/',
         agmparametros_one, name='uno'),
    path('', agmparametros_all, name='todo'),
    path('create/', agmparametros_create, name='create'),
    path('update/', agmparametros_update, name='update'),



]
