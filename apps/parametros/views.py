from django.db import connections
from .models import AgmParametros
from django.db.models import Count, Q
from .serializers import AgmParametrosSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime

@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmparametros_one(request, *args, **kwargs):
    if request.method == 'GET':
        agmparametros = AgmParametros.objects.get(
            id_parametro=kwargs['id_parametro'])
        serializer = AgmParametrosSerializer(agmparametros)
        return Response(serializer.data)

@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmparametros_all(request, *args, **kwargs):
    if request.method == 'GET':
        agmparametros = AgmParametros.objects.all().order_by(
            '-id_parametro')
        serializer = AgmParametrosSerializer(agmparametros, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def agmparametros_create(request):
    try:
        if request.method == 'POST':

            agmparametros = AgmParametros.objects.filter(
                nombre=request.data['nombre'],
                codigo_letras=request.data['codigo_letras'],
                siglas=request.data['siglas'])
            if agmparametros:
                raise Exception('Parametro ya creado en la tabla')

            ultimo = AgmParametros.objects.all().order_by('id_parametro').last()
            serializer = AgmParametrosSerializer(data={
                'id_parametro': ultimo.id_parametro + 1,
                'nombre': request.data['nombre'],
                'codigo_letras': request.data['codigo_letras'],
                'siglas': request.data['siglas'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El Parametro ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear el parametro"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def agmparametros_update(request):
    try:
        if request.method == 'PUT':

            agmparametros = AgmParametros.objects.filter(
                id_parametro=request.data['id_parametro'])
            if not agmparametros:
                raise Exception('not found')

            serializer = AgmParametrosSerializer(
                agmparametros[0],
                data={
                    'id_parametro': request.data['id_parametro'],
                    'nombre': request.data['nombre'],
                    'codigo_letras': request.data['codigo_letras'],
                    'siglas': request.data['siglas']
                }, partial=True)
            if serializer.is_valid():
                # print("validating")
                serializer.save()
                return Response({
                    "message": " El Parametros ha sido actualizado"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar el parametro"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
