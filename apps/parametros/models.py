from django.db import models


class AgmParametros(models.Model):
    id_parametro = models.FloatField(primary_key=True)
    nombre = models.CharField(max_length=100, unique=True)
    codigo_letras = models.CharField(max_length=30, blank=True, null=True)
    siglas = models.CharField(max_length=20, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agm_parametros'
        verbose_name = 'Parametro'
        verbose_name_plural = 'Parametros'
