from .models import AgmParametros
from rest_framework import serializers
from django.db import connections


class AgmParametrosSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgmParametros
        fields = ('id_parametro', 'nombre',
                  'codigo_letras', 'siglas', 'fsistema')
