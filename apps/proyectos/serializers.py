from apps.proyectos.models import VAgmTematicasXMetodologias
from rest_framework import serializers
from .models import VAgmTematicasXMetodologias,AgmMetodologiasxProyecto, ClstProyectos

class VAgmTematicasXMetodologiasSerializer(serializers.ModelSerializer):
    class Meta:
        model = VAgmTematicasXMetodologias
        fields = ("metodologia","codigo","tematica")
        
        
class AgmMetodologiasxProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgmMetodologiasxProyecto
        fields = ("id_metodologia","id_proyecto")
        
        
class ClstProyectosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClstProyectos
        fields =  '__all__'
        
        