from django.urls import path
from apps.proyectos.views import metodologiasProyectoView, clstproyectos_all,clstproyectos_create,clstproyectos_one,clstproyectos_update
from .views import metodologiasProyectoView


urlpatterns = [
    path('metxpro/', metodologiasProyectoView, name='metodologias x proyecto'),
    path('u/<int:codigo>/', clstproyectos_one, name='uno'),
    path('', clstproyectos_all, name='todo'),
    path('create/', clstproyectos_create, name='create'),
    path('update/', clstproyectos_update, name='update'),
    ]