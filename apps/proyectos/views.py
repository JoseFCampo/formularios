from django.db import connections
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.db.models import Count, Q
from .models import VAgmTematicasXMetodologias, AgmMetodologiasxProyecto, ClstProyectos
from .serializers import VAgmTematicasXMetodologiasSerializer, AgmMetodologiasxProyectoSerializer, ClstProyectosSerializer
from apps.metodologias.models import AgmMetodologias
from datetime import datetime

@csrf_exempt
@api_view(['POST'])
#@permission_classes((AllowAny,))
def metodologiasProyectoView(request, *args, **kwargs):
    if request.method == 'POST':
        # print(request.data)
        response = []
        metodologiasProyecto = AgmMetodologiasxProyecto.objects.filter(
            id_proyecto=request.data['codigo']).values_list('id_metodologia', flat=True)
        
        for met in metodologiasProyecto:
            r={}
            metos=AgmMetodologias.objects.filter(id_metodologia=met).values_list('nombre', flat=True)
            r["metodologia"]= metos[0]
            r["id_metodologia"]= met
            r["tematicas_metodologias"]=[]
            tematicas=VAgmTematicasXMetodologias.objects.filter(metodologia=met).values_list("tematica",flat=True)
            for tem in tematicas:
               r["tematicas_metodologias"].append(tem)
            response.append(r)
                
            
    # serializer = AgmMetodologiasxProyectoSerializer(metodologiasProyecto, many=True)
    
    return Response(response)


@api_view(['GET'])
#@permission_classes((AllowAny,))
def clstproyectos_one(request, *args, **kwargs):
    if request.method == 'GET':
        clstproyectos = ClstProyectos.objects.get(
            codigo=kwargs['codigo'])
        serializer = ClstProyectosSerializer(clstproyectos)
        return Response(serializer.data)

@api_view(['GET'])
@permission_classes((AllowAny,))
def clstproyectos_all(request, *args, **kwargs):
    if request.method == 'GET':
        clstproyectos = ClstProyectos.objects.all().order_by(
            'nombre_alterno')
        serializer = ClstProyectosSerializer(clstproyectos, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def clstproyectos_create(request):
    try:
        if request.method == 'POST':

            clstproyectos = ClstProyectos.objects.filter(
                nomobre_alterno=request.data['nombre_alterno'],
                titulo=request.data['titulo'],
                ejecutor=request.data['ejecutor'])
            if clstproyectos:
                raise Exception('Unidad ya creada en la tabla')

            ultimo = ClstProyectos.objects.all().order_by('codigo').last()
            serializer = ClstProyectosSerializer(data={
                'codigo': ultimo.codigo + 1,
                'nombre_alterno': request.data['nombre_alterno'],
                'titulo': request.data['titulo'],
                'ejecutor': request.data['ejecutor'],
                'finicio':request.data['finicio'],
                'ffinalizo':request.data['ffinalizo'],
                'id_metadato_pro':request.data['id_metadato_pro'],
                'sistema':request.data['sistema'],
                'logo_pro':request.data['logo_pro'],
                'css_pro':request.data['css_pro'],
                'resumen':request.data['resumen'],
                'fecha_sistema':datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
                'fcustodia_labsis':request.data['fcustodia_labsis'],
                'codigo_char':request.data['codigo_char'],
                'url_imagen':request.data['url_imagen'] 
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El Parametro ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear el parametro"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def clstproyectos_update(request):
    try:
        if request.method == 'PUT':

            clstproyectos = ClstProyectos.objects.filter(
                codigo=request.data['codigo'])
            if not clstproyectos:
                raise Exception('not found')

            serializer = ClstProyectosSerializer(
                clstproyectos[0],
                data={
                'nombre_alterno': request.data['nombre_alterno'],
                'titulo': request.data['titulo'],
                'ejecutor': request.data['ejecutor'],
                'finicio':request.data['finicio'],
                'ffinalizo':request.data['ffinalizo'],
                'id_metadato_pro':request.data['id_metadato_pro'],
                'sistema':request.data['sistema'],
                'logo_pro':request.data['logo_pro'],
                'css_pro':request.data['css_pro'],
                'resumen':request.data['resumen'],
                'fcustodia_labsis':request.data['fcustodia_labsis'],
                'codigo_char':request.data['codigo_char'],
                'url_imagen':request.data['url_imagen'] 
                }, partial=True)
            if serializer.is_valid():
                # print("validating")
                serializer.save()
                return Response({
                    "message": " El Parametros ha sido actualizado"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar el parametro"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
