from django.db import models
# from apps.metodologias.models import AgmMetodologias
# from apps.tematicas.models import AgmTematicas
# from apps.estaciones.models import ClstProyectos

class VAgmTematicasXMetodologias(models.Model):
    rowid = models.CharField(max_length=50, primary_key=True, default=None, editable=False)
    metodologia = models.ForeignKey(
        'metodologias.AgmMetodologias', models.DO_NOTHING, db_column='metodologia', blank=True, null=True)
    codigo = models.ForeignKey(
        'tematicas.AgmTematicas', models.DO_NOTHING, db_column='codigo', blank=True, null=True)
    tematica=models.CharField(max_length=4000, blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'v_agm_tematicasxmetodologia'
        
class AgmMetodologiasxProyecto(models.Model):
    rowid = models.CharField(max_length=50, primary_key=True, default=None, editable=False)
    id_metodologia = models.ForeignKey(
        'metodologias.AgmMetodologias', models.DO_NOTHING, db_column='id_metodologia', blank=True, null=True)
    id_proyecto = models.ForeignKey(
        'estaciones.ClstProyectos', models.DO_NOTHING, db_column='id_proyecto', blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'agm_metodologiaxproyecto'


class ClstProyectos(models.Model):
    codigo = models.FloatField(primary_key=True)
    titulo = models.CharField(max_length=350)
    nombre_alterno = models.CharField(max_length=100)
    finicio = models.DateField(blank=True, null=True)
    ffinalizo = models.DateField(blank=True, null=True)
    ejecutor = models.CharField(max_length=30, blank=True, null=True)
    id_metadato_pro = models.IntegerField(blank=True, null=True)
    sistema = models.IntegerField(blank=True, null=True)
    logo_pro = models.BinaryField(blank=True, null=True)
    css_pro = models.CharField(max_length=15, blank=True, null=True)
    resumen = models.CharField(max_length=600, blank=True, null=True)
    fecha_sistema = models.DateField(blank=True, null=True)
    fcustodia_labsis = models.DateField(blank=True, null=True)
    codigo_char = models.CharField(unique=True, max_length=15)
    url_imagen = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clst_proyectos'