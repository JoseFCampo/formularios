from django.db import models
from django.db import connections
from datetime import datetime
# Create your models here.


class AgEspecies(models.Model):
    rowid = models.CharField(
        max_length=100, primary_key=True, default=None, editable=False)
    id_proyecto = models.FloatField()
    cod_letras = models.CharField(max_length=30)
    clave_sibm = models.CharField(max_length=30, blank=True, null=True)
    descripcion = models.CharField(max_length=100)
    phylum = models.CharField(max_length=100, blank=True, null=True)
    clase = models.CharField(max_length=100, blank=True, null=True)
    orden = models.CharField(max_length=100, blank=True, null=True)
    familia = models.CharField(max_length=100, blank=True, null=True)
    fechasis = models.DateField(blank=True, null=True)
    genero = models.CharField(max_length=100, blank=True, null=True)
    id_metodologia = models.FloatField()
    notas = models.TextField(max_length=500, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.descripcion)

    class Meta:
        managed = False
        db_table = 'ag_especies'
        unique_together = (('cod_letras', 'id_proyecto', 'id_metodologia'),)
        verbose_name = 'Especie'
        verbose_name_plural = 'Especies'

    # def save(self, *args, **kwargs):
    #     self.fechasis = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    #     with connections['datos'].cursor() as cursor:
    #         cursor.execute("INSERT INTO AG_ESPECIES (id_proyecto,cod_letras,descripcion,clave_sibm,phylum,clase,orden,familia,fechasis,genero,id_metodologia,notas) VALUES ( %s, %s, %s, %s, %s, %s, %s)", [
    #             self.id_proyecto, self.cod_letras, self.descripcion, self.clave_sibm, self.phylum, self.clase, self.orden, self.familia, self.fechasis, self.genero, self.id_metodologia, self.notas])
    #         cursor.execute('SELECT * FROM AG_ESPECIES WHERE id_proyecto=%s AND cod_letras=%s AND id_metodologia=%s',
    #                        [self.id_proyecto, self.cod_letras, self.id_metodologia])
    #         row = cursor.fetchone()
    #         # print(row)
    #     return row
