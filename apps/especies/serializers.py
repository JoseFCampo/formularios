from .models import AgEspecies
from rest_framework import serializers
from django.db import connections


class AgEspeciesSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgEspecies
        fields = ('id_proyecto', 'cod_letras', 'descripcion', 'clave_sibm', 'phylum',
                  'clase', 'orden', 'familia', 'fechasis', 'genero', 'id_metodologia', 'notas')

    def create(self, validated_data):
        with connections['datos'].cursor() as cursor:
            cursor.execute("INSERT INTO AG_ESPECIES (id_proyecto,cod_letras,descripcion,clave_sibm,phylum,clase,orden,familia,fechasis,genero,id_metodologia,notas) VALUES ( %s, %s, %s, %s, %s, %s, %s)", [
                self.id_proyecto, self.cod_letras, self.descripcion, self.clave_sibm, self.phylum, self.clase, self.orden, self.familia, self.fechasis, self.genero, self.id_metodologia, self.notas])
            cursor.execute('SELECT * FROM AG_ESPECIES WHERE id_proyecto=%s AND cod_letras=%s AND id_metodologia=%s',
                           [self.id_proyecto, self.cod_letras, self.id_metodologia])
            row = cursor.fetchone()
        return row

    def update(self, validated_data):
        with connections['datos'].cursor() as cursor:
            cursor.execute('UPDATE AG_ESPECIES SET id_proyecto=%s,cod_letras=%s, descripcion=%s, clave_sibm=%s, phylum=%s, clase=%s orden=%s, familia=%s, fechasis=%s, genero=%s, id_metodologia=%s, notas=%s WHERE id_proyecto=%s AND cod_letras=%s AND id_metodologia=%s', [validated_data['id_proyecto'], validated_data['cod_letras'], validated_data[
                           'descripcion'], validated_data['clave_sibm'], validated_data['phylum'], validated_data['clase'], validated_data['orden'], validated_data['familia'], validated_data['fechasis'], validated_data['genero'], validated_data['id_metodologia'], validated_data['notas'], validated_data['id_proyecto'], validated_data['cod_letras'], validated_data['id_metodologia']])

            cursor.execute('SELECT * FROM AG_ESPECIES WHERE id_proyecto=%s AND cod_letras=%s AND id_metodologia=%s',
                           [validated_data['id_proyecto'], validated_data['cod_letras'], validated_data['id_metodologia']])
            row = cursor.fetchone()
        return row
