from apps.especies.views import(
    agespecies_one,
    agespecies_create,
    agespecies_update,
    agespecies_all,
    agespecies_filt,
    proy_filt,
    metod_filt,
    MyView
)
from django.urls import path


urlpatterns = [
    path('proy/',  proy_filt, name='proyecto'),
    path('metd/<int:id_proyecto>/',  metod_filt, name='filtrometo'),
    path('filt/<int:id_proyecto>,<int:id_metodologia>/',
         agespecies_filt, name='filt'),
    path('u/<int:id_proyecto>,<str:cod_letras>,<int:id_metodologia>/',
         agespecies_one, name='uno'),
    path('', agespecies_all, name='todo'),
    path('create/', agespecies_create, name='create'),
    path('update/', agespecies_update, name='update'),
    path('excel/', MyView, name="excel")



]
