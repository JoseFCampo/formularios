from django.contrib import admin

from apps.especies.models import AgEspecies
# Register your models here.


class AgEspeciesAdmin(admin.ModelAdmin):
    def delete(self, obj):
        return '<input type="button" value="Borrar" onclick="location.href=\'{0}/delete/\'" />'.format(obj.pk)

    list_display = ('id_proyecto', 'cod_letras', 'descripcion', 'clave_sibm', 'phylum',
                    'clase', 'orden', 'familia', 'fechasis', 'genero', 'id_metodologia', 'notas')
    list_display_links = ('id_proyecto', 'cod_letras', 'descripcion', 'clave_sibm',
                          'phylum', 'clase', 'orden', 'familia', 'fechasis', 'genero')
    search_fields = ['id_proyecto', 'cod_letras', 'descripcion',
                     'clave_sibm', 'phylum', 'clase', 'orden', 'familia',  'id_metodologia', 'notas', 'genero', ]

    list_filter = ['phylum', 'clase', 'orden',
                   'familia', 'genero', 'id_proyecto', 'id_metodologia']


admin.site.register(AgEspecies, AgEspeciesAdmin)
