from django.db import connections
from django.db.models import Count, Q
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from datetime import datetime
from .models import AgEspecies
from apps.especies.serializers import AgEspeciesSerializer
from apps.estaciones.models import ClstProyectos
from apps.estaciones.serializers import ClstProyectosSerializer
from apps.metodologias.models import AgmMetodologias
from apps.metodologias.serializers import AgmMetodologiasSerializer
from django.http import HttpResponse
from django.views.generic import View
import io
import xlsxwriter


@api_view(['GET'])
@permission_classes((AllowAny,))
def agespecies_one(request, *args, **kwargs):
    if request.method == 'GET':
        agEspecies = AgEspecies.objects.get(
            id_proyecto=kwargs['id_proyecto'], cod_letras=kwargs['cod_letras'], id_metodologia=kwargs['id_metodologia'])
        serializer = AgEspeciesSerializer(agEspecies)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def metod_filt(request, *args, **kwargs):
    if request.method == 'GET':
        if kwargs['id_proyecto'] == 0:
            metd = AgEspecies.objects.all().order_by('id_metodologia').values_list(
                'id_metodologia', flat=True).distinct()
            nmet = AgmMetodologias.objects.filter(
                id_metodologia__in=metd).order_by('nombre')
            serializer = AgmMetodologiasSerializer(nmet, many=True)
        else:
            metd = AgEspecies.objects.order_by('id_metodologia').filter(id_proyecto__exact=kwargs['id_proyecto']).values_list(
                'id_metodologia', flat=True).distinct()
            nmet = AgmMetodologias.objects.filter(
                id_metodologia__in=metd).order_by('nombre')
            serializer = AgmMetodologiasSerializer(nmet, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def proy_filt(request, *args, **kwargs):
    def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    if request.method == 'GET':
        with connections['datos'].cursor() as cursor:
            cursor.execute('SELECT distinct id_proyecto from ag_especies'
                           )
            response = dictfetchall(cursor)
        res = []
        for l in response:
            res.append(l['ID_PROYECTO'])
        proyectos = ClstProyectos.objects.filter(codigo__in=res)
        serializer = ClstProyectosSerializer(proyectos, many=True)
        return Response(serializer.data)


@ api_view(['GET'])
@permission_classes((AllowAny,))
def agespecies_filt(request, *args, **kwargs):
    if request.method == 'GET':
        if kwargs['id_proyecto'] == 0:
            if kwargs['id_metodologia'] == 0:
                agespecies = AgEspecies.objects.all().order_by(
                    'id_proyecto', 'cod_letras', 'id_metodologia')
            else:
                agespecies = AgEspecies.objects.filter(id_metodologia__exact=kwargs['id_metodologia']).order_by(
                    'id_proyecto', 'cod_letras', 'id_metodologia')
        else:
            if kwargs['id_metodologia'] == 0:
                agespecies = AgEspecies.objects.filter(id_proyecto__exact=kwargs['id_proyecto']).order_by(
                    'id_proyecto', 'cod_letras', 'id_metodologia')
            else:
                agespecies = AgEspecies.objects.filter(id_proyecto__exact=kwargs['id_proyecto'], id_metodologia__exact=kwargs['id_metodologia']).order_by(
                    'id_proyecto', 'cod_letras', 'id_metodologia')

        serializer = AgEspeciesSerializer(agespecies, many=True)
        return Response(serializer.data)


@ api_view(['GET'])
@permission_classes((AllowAny,))
def agespecies_all(request, *args, **kwargs):
    if request.method == 'GET':
        agespecies = AgEspecies.objects.all().order_by(
            '-id_proyecto', '-cod_letras', '-id_metodologia')
        serializer = AgEspeciesSerializer(agespecies, many=True)
        return Response(serializer.data)


@ api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def agespecies_create(request):
    try:
        if request.method == 'POST':
            agEspecies = AgEspecies.objects.get(
                id_proyecto=request.data['id_proyecto'], cod_letras=request.data['cod_letras'], id_metodologia=request.data['id_metodologia'])
            if agEspecies:
                raise Exception('Parametro creado en la tabla')

            serializer = AgEspeciesSerializer(data={
                'id_proyecto': request.data['id_proyecto'],
                'cod_letras': request.data['cod_letras'],
                'descripcion': request.data['descripcion'],
                'clave_sibm': request.data['clave_sibm'],
                'phylum': request.data['phylum'],
                'clase': request.data['clase'],
                'orden': request.data['orden'],
                'familia': request.data['familia'],
                'fechasis': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
                'genero': request.data['genero'],
                'id_metodologia': request.data['id_metodologia'],
                'notas': request.data['notas'],
            }, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La especie ha sido creada"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear la especie"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
@permission_classes((IsAuthenticated, ))
def agespecies_update(request):
    try:
        if request.method == 'PUT':

            agEspecies = AgEspecies.objects.filter(
                id_proyecto=request.data['id_proyecto'], cod_letras=request.data['cod_letras'], id_metodologia=request.data['id_metodologia'])
            if not agEspecies:
                raise Exception('not found')

            serializer = AgEspeciesSerializer(agEspecies[0], data={
                'id_proyecto': request.data['id_proyecto'],
                'cod_letras': request.data['cod_letras'],
                'descripcion': request.data['descripcion'],
                'clave_sibm': request.data['clave_sibm'],
                'phylum': request.data['phylum'],
                'clase': request.data['clase'],
                'orden': request.data['orden'],
                'familia': request.data['familia'],
                'genero': request.data['genero'],
                'id_metodologia': request.data['id_metodologia'],
                'notas': request.data['notas'],
            }, partial=True)

            print(serializer)
            if serializer.is_valid():
                serializer.save() 
                print(serializer.data)
                # serializer.update({
                #     'id_proyecto': request.data['id_proyecto'],
                #     'cod_letras': request.data['cod_letras'],
                #     'descripcion': request.data['descripcion'],
                #     'clave_sibm': request.data['clave_sibm'],
                #     'phylum': request.data['phylum'],
                #     'clase': request.data['clase'],
                #     'orden': request.data['orden'],
                #     'familia': request.data['familia'],
                #     'genero': request.data['genero'],
                #     'id_metodologia': request.data['id_metodologia'],
                #     'notas': request.data['notas'],
                # })
                return Response({
                    "message": " La especie ha sido actualizada"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": "Fallo al actualizar la especie"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


def get_simple_table_data(kwargs):
    if kwargs['id_proyecto'] == 0:
        if kwargs['id_metodologia'] == 0:
            agespecies = AgEspecies.objects.all().order_by(
                'id_proyecto', 'cod_letras', 'id_metodologia')
        else:
            agespecies = AgEspecies.objects.filter(id_metodologia__exact=kwargs['id_metodologia']).order_by(
                'id_proyecto', 'cod_letras', 'id_metodologia')
    else:
        if kwargs['id_metodologia'] == 0:
            agespecies = AgEspecies.objects.filter(id_proyecto__exact=kwargs['id_proyecto']).order_by(
                'id_proyecto', 'cod_letras', 'id_metodologia')
        else:
            agespecies = AgEspecies.objects.filter(id_proyecto__exact=kwargs['id_proyecto'], id_metodologia__exact=kwargs['id_metodologia']).order_by(
                'id_proyecto', 'cod_letras', 'id_metodologia')


    return agespecies.values_list('id_proyecto',
                                  'id_metodologia',
                                  'descripcion',
                                  'cod_letras',
                                  'clave_sibm',
                                  'phylum',
                                  'clase',
                                  'orden',
                                  'familia',
                                  'genero',
                                  'notas',
                                  named=True)


@ api_view(['POST'])
@permission_classes((AllowAny,))
def MyView(request, *args, **kwargs):
    if request.method == 'POST':

        # Create an in-memory output file for the new workbook.
        output = io.BytesIO()

        # Even though the final file will be in memory the module uses temp
        # files during assembly for efficiency. To avoid this on servers that
        # don't allow temp files, for example the Google APP Engine, set the
        # 'in_memory' Workbook() constructor option as shown in the docs.
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet()
        bold = workbook.add_format(
            {'bold': True, 'align': 'center', 'align': 'vcenter'})

        # Get some data to write to the spreadsheet.
        worksheet.set_column('A:A', 11)
        worksheet.set_column('B:B', 14)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:K', 16)
        data = get_simple_table_data(request.data)

        worksheet.write('A1', 'Id Proyecto', bold)
        worksheet.write('B1', 'Id Metodologia', bold)
        worksheet.write('C1', 'Descripcion', bold)
        worksheet.write('D1', 'Codigo Alfabetico', bold)
        worksheet.write('E1', 'Clave SIBM', bold)
        worksheet.write('F1', 'Phylum', bold)
        worksheet.write('G1', 'Clase', bold)
        worksheet.write('H1', 'Orden', bold)
        worksheet.write('I1', 'Familia', bold)
        worksheet.write('J1', 'Genero', bold)
        worksheet.write('K1', 'Notas', bold)
        # Write some test data.
        cell_format = workbook.add_format(
            {'bold': False, 'align': 'left', 'align': 'vjustify'})
        for row_num, columns in enumerate(data):
            for col_num, cell_data in enumerate(columns):
                worksheet.write(row_num+1, col_num, cell_data, cell_format)

            # Close the workbook before sending the data.
        workbook.close()

        # Rewind the buffer.
        output.seek(0)

        # Set up the Http response.
        filename = 'django_simple.xlsx'
        response = HttpResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        return response
