from .models import AgmMetodologias
from rest_framework import serializers
from django.db import connections


class AgmMetodologiasSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgmMetodologias
        fields = ('id_metodologia', 'nombre', 'descripcion', 'des_muestras',
                  'fsistema',)
