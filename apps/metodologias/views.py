from django.db import connections
from .models import AgmMetodologias
from django.db.models import Count, Q
from apps.metodologias.serializers import AgmMetodologiasSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime

@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmmetodologias_one(request, *args, **kwargs):
    if request.method == 'GET':
        agmmetodologias = AgmMetodologias.objects.get(
            id_metodologia=kwargs['id_metodologia'],)
        serializer = AgmMetodologiasSerializer(agmmetodologias)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def agmmetodologias_all(request, *args, **kwargs):
    if request.method == 'GET':
        agmmetodologia = AgmMetodologias.objects.all().order_by(
            '-id_metodologia', '-nombre')
        serializer = AgmMetodologiasSerializer(agmmetodologia, many=True)
        return Response(serializer.data)

@api_view(['POST'])
def agmmetodologias_create(request):
    try:
        if request.method == 'POST':

            ultimo = AgmMetodologias.objects.all().order_by(
                '-id_metodologia')[0]
            serializer = AgmMetodologiasSerializer(data={
                'id_metodologia':  ultimo.id_metodologia+1,
                'nombre': request.data['nombre'],
                'descripcion': request.data['descripcion'],
                'des_muestras': request.data['des_muestras'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
            }, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La metodologia ha sido creada"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear la metodologia"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@api_view(['PUT'])
def agmmetodologias_update(request):
    try:
        if request.method == 'PUT':

            agmmetodologias = AgmMetodologias.objects.get(
                id_metodologia=request.data['id_metodologia'])
            if not agmmetodologias:
                raise Exception('not found')

            serializer = AgmMetodologiasSerializer(agmmetodologias[0], data={
                'id_metodologia': request.data['id_metodologia'],
                'nombre': request.data['nombre'],
                'descripcion': request.data['descripcion'],
                'des_muestras': request.data['des_muestras'],
            }, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La metodologia ha sido creada"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al crear la metodologia"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
