from django.contrib import admin

from apps.metodologias.models import AgmMetodologias
# Register your models here.


class AgmMetodologiasAdmin(admin.ModelAdmin):

    list_display = ('id_metodologia', 'des_muestras', 'fvigente_desde',
                    'descripcion', 'version_plantilla', 'nombre', 'fsistema',)
    list_display_links = ('id_metodologia', 'des_muestras', 'fvigente_desde',
                          'descripcion', 'version_plantilla', 'nombre', 'fsistema',)
    search_fields = ['id_metodologia', 'des_muestras',
                     'fvigente_desde', 'descripcion', 'version_plantilla', 'nombre', ]


admin.site.register(AgmMetodologias, AgmMetodologiasAdmin)
