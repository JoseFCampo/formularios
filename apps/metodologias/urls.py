from apps.metodologias.views import(
    agmmetodologias_one,
    agmmetodologias_create,
    agmmetodologias_update,
    agmmetodologias_all,
)
from django.urls import path


urlpatterns = [
    path('u/<int:id_metodologia>/', agmmetodologias_one, name='metodologia'),
    path('', agmmetodologias_all, name='todo'),
    path('create/', agmmetodologias_create, name='create'),
    path('update/', agmmetodologias_update, name='update'),



]
