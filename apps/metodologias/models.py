from django.db import models


class AgmMetodologias(models.Model):
    id_metodologia = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=3000, blank=True, null=True)
    des_muestras = models.CharField(max_length=3000, blank=True, null=True)
    fsistema = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agm_metodologias'
        verbose_name = 'Metodologia'
        verbose_name_plural = 'Metodologias'

class AglMetodologiaEquivalente(models.Model):
    id_metodologia = models.IntegerField(primary_key=True)
    metodologia_equi= models.IntegerField(blank=False, null=False)
    fsistema = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agl_metodologia_equivalente'
        verbose_name = 'Metodologia Equivalente'
        verbose_name_plural = 'Metodologias Equivalentes'
