from django.db import connections
from django.db.models import Count, Q
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime
from rest_framework.views import set_rollback
from apps.estaciones.models import ClstProyectos
from apps.estaciones.serializers import ClstProyectosSerializer
from apps.metodologias.models import AgmMetodologias
from apps.metodologias.serializers import AgmMetodologiasSerializer
from django.http import HttpResponse
import io
import xlsxwriter


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@api_view(['GET', 'POST'])
@permission_classes((AllowAny,))
def proy_filt(request, *args, **kwargs):

    if request.method == 'GET':
        with connections['datos'].cursor() as cursor:
            cursor.execute(
                "SELECT DISTINCT table_name FROM USER_TAB_COLUMNS WHERE REGEXP_LIKE(table_name, '^\VM_AGM_+\d{1,3}+_+\d{1,4}$') order by table_name")
            response = dictfetchall(cursor)

        res = {}
        for resp in response:

            buff = resp['TABLE_NAME'].strip('VM_AGM_').split('_')
            if 'id_proyecto' in res:
                if buff[0] not in res['id_proyecto']:
                    res['id_proyecto'].append(buff[0])
            else:
                res['id_proyecto'] = [buff[0]]

        proyectos = ClstProyectos.objects.order_by('nombre_alterno').filter(codigo__in=res['id_proyecto'])
        serializer = ClstProyectosSerializer(proyectos, many=True)

        return Response(serializer.data)

    if request.method == 'POST':
        with connections['datos'].cursor() as cursor:
            query = "SELECT DISTINCT table_name FROM USER_TAB_COLUMNS WHERE REGEXP_LIKE(table_name, \'^\VM_AGM_"
            query += request.data['id_proyecto']
            query += "_+\d{1,4}$\') order by table_name"
            cursor.execute(query)
            response = dictfetchall(cursor)
            res = []
            metos=[]
        for resp in response:

            buff = resp['TABLE_NAME'].strip('VM_AGM_').split('_')
            res.append({'id_metodologia':buff[1]})
            metos.append(buff[1])
        nmet = list(AgmMetodologias.objects.order_by('nombre').values_list('id_metodologia',flat=True).filter(
            id_metodologia__in=metos))
        for r in res:
            if nmet.count(int(r['id_metodologia']))==1 :
                
                serializer = AgmMetodologiasSerializer(AgmMetodologias.objects.get(id_metodologia=r['id_metodologia']))
                r['data']= serializer.data
            else: 
                 r['data']= []
       
        return Response(res)

@api_view(['POST'])
@permission_classes((AllowAny,))
def est_filt(request, *args, **kwargs):

    if request.method == 'POST':
        with connections['datos'].cursor() as cursor:
            query= "SELECT MIN(FECHA_MES) FROM VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            cursor.execute(query)
            # print(cursor.fetchone())
            
            
            query = "SELECT DISTINCT ID_ESTACION FROM VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += " ORDER BY ID_ESTACION "
            cursor.execute(query)
            response = dictfetchall(cursor)
            res = ""
            for r in response:
                if len(res) >= 1:
                    res += ',' + str(r['ID_ESTACION'])
                else:
                    res = str(r['ID_ESTACION'])
                    
        with connections['geograficos'].cursor() as cursor:
            query = "SELECT DISTINCT ID_ESTACION, PREFIJO_CDG_EST_LOC, LUGAR, TIPO_NODO_DES FROM CLOCALIDADT WHERE PROYECTO_LOC = "
            query += request.data['id_proyecto']
            query += " ORDER BY ID_ESTACION"
            cursor.execute(query)
            response = dictfetchall(cursor)
            resp = []
            for r in response:
                idE = str(r['ID_ESTACION'])
                buff = "(" + str(r['TIPO_NODO_DES']) + ') '
                buff += "Prefijo: "
                buff += str(r['PREFIJO_CDG_EST_LOC']) + \
                    " Lugar: " + str(r['LUGAR']) + " ID: " + idE
                resp.append({'value': r['ID_ESTACION'], 'label': buff})
        response2 = {}
        response2['Estaciones'] = resp
        with connections['datos'].cursor() as cursor:
            query = "SELECT COLUMN_NAME, COLUMN_ID FROM USER_TAB_COLUMNS WHERE table_name LIKE \'VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += "'"
            cursor.execute(query)
            response = dictfetchall(cursor)
            response2['Columnas'] = response
            
            for e in response:
                if 'FACTUALIZACION'== e["COLUMN_NAME"]:
                    query= "SELECT Max(FACTUALIZACION) as act FROM VM_AGM_"
                    query += request.data['id_proyecto']
                    query += "_"
                    query += request.data['id_metodologia']
                    cursor.execute(query)
                    x=dictfetchall(cursor)
                    response2['FechaActualizacion']= x[0]['ACT'].strftime('%Y/%m/%d')
                        
            query = "SELECT NOMBRE_COLUMNA as COLUMN_NAME, TIPO_DATO, TABLA, CONJUNTO FROM AGM_CAMPO WHERE NOMBRE_COLUMNA in (SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME LIKE \'VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += "')"
            query += "AND AGM_CAMPO.ID_METODOLOGIA = "
            query += request.data['id_metodologia']
            cursor.execute(query)
            response = dictfetchall(cursor)
            for r in response:
                if r['TIPO_DATO'] == 4:
                    query = "SELECT CODIGO, DESCRIPCION FROM VM_AG_GENERAL_LOV WHERE TABLA = \'"
                    query += str(r['TABLA'])
                    query += "' AND CONJUNTO = '"
                    query += str(r['CONJUNTO'])
                    query += "'"
                    cursor.execute(query)
                    prueba = dictfetchall(cursor)
                    response[response.index(r)]['OPCIONES'] = prueba
            response2['Columnas_filtros'] = response

        return Response(response2)

@api_view(['POST'])
@permission_classes((AllowAny,))
def opc_filt(request, *args, **kwargs):
    if request.method == 'POST':
        with connections['datos'].cursor() as cursor:
            query = "SELECT CODIGO, DESCRIPCION FROM VM_AG_GENERAL_LOV WHERE TABLA = '"
            query += str(request.data["Tabla"])
            query += "' AND CONJUNTO = '"
            query += str(request.data["Conjunto"])
            query += "'"
            cursor.execute(query)
            res = cursor.fetchall()
        return Response(res)

@api_view(['POST'])
@permission_classes((AllowAny,))
def data_view(request, *args, **kwargs):

    def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    if request.method == 'POST':
        estacionesM = request.data['id_estaciones'].rsplit(",")

        with connections['datos'].cursor() as cursor:
            query = "SELECT "
            query += request.data['columnas']
            query += ", COUNT(*) as COUNT ,COUNT(ID_ESTACION) as COUNT_ESTACIONES FROM VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += " WHERE ID_ESTACION IN ("

            if len(estacionesM) <= 999:
                query += request.data['id_estaciones']
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"
                cursor.execute(query)
                res = {}
                response = dictfetchall(cursor)
                res['Datos'] = response
                query = "SELECT COUNT( distinct ID_ESTACION ) as COUNT_ESTACIONES FROM VM_AGM_"
                query += request.data['id_proyecto']
                query += "_"
                query += request.data['id_metodologia']
                query += " WHERE ID_ESTACION IN ("
                query += request.data['id_estaciones']
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' ORDER BY ID_ESTACION"
                cursor.execute(query)
                buff4=dictfetchall(cursor)
                res['Count_estaciones'] = int(buff4[0]['COUNT_ESTACIONES'])

            else:
                x = estacionesM[0]
                fin = int((len(estacionesM)/2))
                for e in estacionesM[1:fin]:
                    x += ',' + e
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"
                cursor.execute(query)
                res = {}
                response = dictfetchall(cursor)
                res['Datos'] = response
                query = "SELECT COUNT( distinct ID_ESTACION ) as COUNT_ESTACIONES FROM VM_AGM_"
                query += request.data['id_proyecto']
                query += "_"
                query += request.data['id_metodologia']
                query += " WHERE ID_ESTACION IN ("
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' ORDER BY ID_ESTACION"
                cursor.execute(query)
                buff4=dictfetchall(cursor)
                res['Count_estaciones'] = int(buff4[0]['COUNT_ESTACIONES'])
                query = "SELECT "
                query += request.data['columnas']
                query += ", COUNT(*) as COUNT, COUNT(ID_ESTACION) as COUNT_ESTACIONES FROM VM_AGM_"
                query += request.data['id_proyecto']
                query += "_"
                query += request.data['id_metodologia']
                query += " WHERE ID_ESTACION IN ("
                x = estacionesM[fin]
                for e in estacionesM[fin+1:len(estacionesM)]:
                    x += ',' + e
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"
                cursor.execute(query)
                response = dictfetchall(cursor)
                for resp in response:
                    res['Datos'].append(resp)
                    
                query = "SELECT COUNT( distinct ID_ESTACION ) as COUNT_ESTACIONES FROM VM_AGM_"
                query += request.data['id_proyecto']
                query += "_"
                query += request.data['id_metodologia']
                query += " WHERE ID_ESTACION IN ("
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' ORDER BY ID_ESTACION"
                cursor.execute(query)
                buff4=dictfetchall(cursor)
                res['Count_estaciones'] = res['Count_estaciones']+int(buff4[0]['COUNT_ESTACIONES'])   
            
            co = 0
            for re in res['Datos']:
                co += int(re['COUNT'])

            res['Total_registros'] = co
            
            

            query = "SELECT  COUNT(*) as TOTAL_TABLA FROM VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            cursor.execute(query)
            response = dictfetchall(cursor)
            res['Total_tabla'] = response[0]['TOTAL_TABLA']

        return Response(res)

@api_view(['POST'])
@permission_classes((AllowAny,))
def export_view(request, *args, **kwargs):

    if request.method == 'POST':
        estacionesM = request.data['id_estaciones'].rsplit(",")
        with connections['datos'].cursor() as cursor:
            query = "SELECT COLUMN_NAME, COLUMN_ID FROM USER_TAB_COLUMNS WHERE table_name LIKE \'VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += "'"
            cursor.execute(query)
            response = dictfetchall(cursor)
            FechaActualizacion=''
            for e in response:
                if 'FACTUALIZACION'== e["COLUMN_NAME"]:
                    query= "SELECT Max(FACTUALIZACION) as act FROM VM_AGM_"
                    query += request.data['id_proyecto']
                    query += "_"
                    query += request.data['id_metodologia']
                    cursor.execute(query)
                    x=dictfetchall(cursor)
                    FechaActualizacion= x[0]['ACT'].strftime('%Y/%m/%d')


            query = "SELECT "
            query += request.data['columnas']
            query += ", COUNT(*) as COUNT FROM VM_AGM_"
            query += request.data['id_proyecto']
            query += "_"
            query += request.data['id_metodologia']
            query += " WHERE ID_ESTACION IN ("

            if len(estacionesM) <= 999:
                query += request.data['id_estaciones']
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"
                cursor.execute(query)
                response = cursor.fetchall()

            else:
                x = estacionesM[0]
                fin = int((len(estacionesM)/2))
                for e in estacionesM[1:fin]:
                    x += ',' + e
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"
                cursor.execute(query)
                response = cursor.fetchall()
                query = "SELECT "
                query += request.data['columnas']
                query += ", COUNT(*) as COUNT FROM VM_AGM_"
                query += request.data['id_proyecto']
                query += "_"
                query += request.data['id_metodologia']
                query += " WHERE ID_ESTACION IN ("
                x = estacionesM[fin]
                for e in estacionesM[fin+1:len(estacionesM)]:
                    x += ',' + e
                query += x
                query += ")"
                query += request.data['adicionales']
                query += " AND FECHA_MES BETWEEN '"
                query += request.data['fecha_inicio']
                query += "' AND '"
                query += request.data['fecha_final']
                query += "' GROUP BY ("
                query += request.data['columnas']
                query += ") ORDER BY ID_ESTACION"

                cursor.execute(query)
                res = cursor.fetchall()
                for resp in res:
                    response.append(resp)

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output)
        sh = 'VM_AGM_'+request.data['id_proyecto'] + \
            '_'+request.data['id_metodologia']
        worksheet = workbook.add_worksheet(sh)
        data = response
        bold_format = workbook.add_format(
            {'bold': True, 'align': 'center', 'align': 'vjustify'})

        cell_format = workbook.add_format(
            {'bold': False, 'align': 'center', 'align': 'vjustify'})
        worksheet.merge_range(0, 0, 0, 1,
                              'Nombre del Proyecto', bold_format)

        worksheet.merge_range(1, 0, 1, 1,
                              'Nombre de la Metodologia', bold_format)
        worksheet.merge_range(2, 0, 2, 1,
                              'Fecha de Consulta', bold_format)
       

        worksheet.merge_range(0, 2,0, 3, request.data['nombre_proyecto'], cell_format)
        worksheet.merge_range(1, 2,1, 3, request.data['nombre_metodologia'], cell_format)
        worksheet.merge_range(2, 2,2, 3, request.data['fconsulta'], cell_format)
        if (FechaActualizacion!=''):
            worksheet.merge_range(3, 0, 3, 1,
                              'Fecha de Actualizacion', bold_format)
            worksheet.merge_range( 3, 2,3, 3, FechaActualizacion , cell_format)

        columns1 = request.data['columnas'].split(',')
        columns1.append("NO. REGISTROS")

        for col_num, cell_data in enumerate(columns1):
            worksheet.set_column(col_num, col_num, len(cell_data)+4)
            if ('FECHA' in cell_data and (len(cell_data)+4) < 12):
                worksheet.set_column(col_num, col_num, 12)
            if (cell_data in ['PARTICIPANTES', 'NOM_ESTACION']):
                worksheet.set_column(col_num, col_num, 30)
            worksheet.write(5, col_num, cell_data, bold_format)

        row_num=0
        for row_num, columns in enumerate(data):
            for col_num, cell_data in enumerate(columns):
                if type(cell_data) == datetime:
                    date_format = workbook.add_format(
                        {'bold': False, 'align': 'rigth', 'align': 'vjustify', 'num_format': 'dd/mm/yyyy'})
                    worksheet.write_datetime(
                        row_num+6, col_num, cell_data, date_format)
                else:
                    worksheet.write(row_num+6, col_num, cell_data, cell_format)

        worksheet.merge_range(row_num+8, 0, row_num+8, 2, 'Total de Estaciones Consultadas', bold_format)

        worksheet.merge_range(row_num+9, 0, row_num+9, 2, 'Total de Registros devueltos', bold_format)
        worksheet.merge_range(row_num+10, 0, row_num+10, 2,'Total de Datos Almacenados', bold_format)

        worksheet.write(row_num+8, 3, request.data['count_estaciones'], cell_format)
        worksheet.write(row_num+9, 3, request.data['total_registros'], cell_format)
        worksheet.write(row_num+10, 3, request.data['total_tabla'], cell_format)

        workbook.close()

        
        output.seek(0)

        filename = 'datos.xlsx'
        response = HttpResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename

        return response
