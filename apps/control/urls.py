from apps.control.views import(
    proy_filt,
    est_filt,
    data_view,
    export_view,
    opc_filt
)
from django.urls import path


urlpatterns = [
    path('proy/',  proy_filt, name='proyecto'),
    path('est/',  est_filt, name='estaciones'),
    path('data/',  data_view, name='estaciones'),
    path('export/',  export_view, name='estaciones'),
    path('filt/', opc_filt, name='opciones'),
]
