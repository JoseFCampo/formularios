import os
from django.db import connections
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from datetime import datetime
from apps.estaciones.models import ClstProyectos
from apps.estaciones.serializers import ClstProyectosSerializer
from .models import (UrolesUsuarios1, Usuarios, UsuariosDetalles, VPermisosRoles,
                     UrolesUsuarios, Uroles, DatosUsuarios, UpermisosRoles, UcodigosLov)
from .serializers import (UpermisosRolesSerializer, UrolesUsuariosSerializer, UrolesUsuariosSerializer1, UsuariosDetallesSerializer,
                          UsuariosSerializer, Usuarios2Serializer, DatosUsuariosSerializer,
                          UrolesSerializer, UcodigosLovSerializer)
import jwt
from django.conf import settings



from rest_framework import exceptions
from django.views.decorators.csrf import ensure_csrf_cookie
from .utils import generate_access_token, generate_refresh_token
from rest_framework import parsers, renderers, status
from rest_framework.authtoken.serializers import AuthTokenSerializer
import hashlib


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

##########VISTAS USUARIO LOGEADO ############


@api_view(('POST',))
@permission_classes((AllowAny, ))
def UserProjects(request):
    if request.method == 'POST':
        if request.data:
            id_proyectos = request.data
            proyectos = ClstProyectos.objects.filter(codigo__in=id_proyectos)
            serializer = ClstProyectosSerializer(proyectos, many=True)
            return Response(serializer.data)


@api_view(('GET',))
@permission_classes((IsAuthenticated, ))
def userDetallesView(request, *args, **kwargs):
    if request.method == 'GET':
        detalles = UsuariosDetalles.objects.filter(
            id_usuario=kwargs['id_usuario'])
        serializer = UsuariosDetallesSerializer(detalles, many=True)
        return Response(serializer.data)


@api_view(('POST',))
@permission_classes((AllowAny,))
def LoginView(request):
    if request.method == 'POST':
        print(request.data)
        username = request.data['username']
        password = request.data['password']
        print(username)
        print(password)

        try:
            user_db = Usuarios.objects.filter(username_email=username).last()
            print(user_db)
        except Exception as err:
            return Response({
                "message": str(err)
            }, status=status.HTTP_400_BAD_REQUEST)

        if user_db:
            if user_db.activo == 'S':
                h = hashlib.new('sha1', password.encode())

                if user_db.password == str(h.hexdigest()):
                    uroles = UrolesUsuarios.objects.filter(
                        id_usuario=user_db.id)
                    roles = Uroles.objects.filter(id__in=uroles)
                    info = Usuarios.objects.filter(username_email=username).values(
                        'id', 'nombre', 'username_email').last()
                    info["roles"] = roles.order_by('id_proyecto').values(
                        'id', 'descripcion', 'id_proyecto')
                    info["proyectos"] = Uroles.objects.filter(id__in=uroles).order_by(
                        'id_proyecto').values_list('id_proyecto', flat=True).distinct()
                    SECRET_KEY = settings.SECRET_KEY
                    # encoded = jwt.encode(
                    #     {'id': user_db.id}, SECRET_KEY, algorithm='HS256')

                    access_token = generate_access_token(user_db)
                    refresh_token = generate_refresh_token(user_db)

                    response = Response({
                        "access": access_token,
                        "userInfo": info},
                        status=status.HTTP_202_ACCEPTED)

                    response.set_cookie(key='refreshtoken',
                                        value=refresh_token, httponly=True)
                    return response
                else:
                    return Response({
                        "message": 'Contraseña Incorrecta'
                    }, status=status.HTTP_409_CONFLICT)

            else:
                return Response({
                    "message": 'Usuario Inactivo'
                }, status=status.HTTP_404_NOT_FOUND)

        else:
            return Response({
                "message": 'Usuario no existe'
            }, status=status.HTTP_404_NOT_FOUND)

#########VISTAS DE USUARIOS DEL PROYECTO################


@api_view(['GET'])
# @permission_classes((AllowAny,))
def usersViewOne(request, *args, **kwargs):
    def dictfetchall(cursor):
        columns = [col[0].lower() for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    if request.method == 'GET':
        with connections['usuarios_sci'].cursor() as cursor:
            query = 'SELECT distinct id, id_rol, descripcion, username_email, nombre, apellido, activo, id_proyecto from datos_usuarios where id = '
            query += str(kwargs['id_usuario'])
            query += 'AND id_proyecto = '
            query += str(kwargs['id_proyecto'])
            cursor.execute(query)
            response = dictfetchall(cursor)
            serializer = DatosUsuariosSerializer(response[0], partial=True)
        # print(serializer.data)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def usersViewAll(request, *args, **kwargs):
    def dictfetchall(cursor):
        columns = [col[0].lower() for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    if request.method == 'GET':
        with connections['usuarios_sci'].cursor() as cursor:
            query = 'SELECT distinct id as id_usuario, id_rol, descripcion, username_email, nombre, apellido, activo, id_proyecto from datos_usuarios where id_proyecto = '
            query += str(kwargs['id_proyecto'])
            query+=' order by nombre ASC'
            cursor.execute(query)
            response = dictfetchall(cursor)
            serializer = DatosUsuariosSerializer(
                response, many=True, partial=True)
            # print(serializer.data)
        return Response(serializer.data)


@api_view(('POST',))
def UserRegisterView(request):
    try:
        if request.method == 'POST':
            usuario = Usuarios.objects.filter(
                username_email=request.data['username_email'],)
            if usuario:
                raise Exception('Usuario ya registrado en el sistema')

            h = hashlib.new('sha1', request.data['password'].encode())
            serializer = UsuariosSerializer(data={
                'username_email': request.data['username_email'],
                'password': str(h.hexdigest()),
                'llavepassword': request.data['llavepassword'],
                'nombre': request.data['nombre'],
                'apellido': request.data['apellido'],
                'activo':  request.data['activo'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
            }, partial=True)

            
            if serializer.is_valid(): 
                serializer.save()
                
                ultimo=Usuarios.objects.filter(username_email=request.data['username_email'], password=str(h.hexdigest())).order_by('id')[0]
                with connections['usuarios_sci'].cursor() as cursor:
                    query =  "INSERT INTO USUARIOS_DETALLES (id_usuario,codigo_atributo,valor_asignado) VALUES ("
                    query += str(ultimo.id)
                    query+=", 17,'CO')"
                    cursor.execute(query)
                serializer2 = UrolesUsuariosSerializer1(data={
                'id_rol': int(request.data['rol']),
                'id_usuario': int(ultimo.id)
                })
                if serializer2.is_valid():
                    serializer2.save()
                    return Response({
                        "message": "Se ha creado el usuario satisfactoriamente"
                    }, status=status.HTTP_201_CREATED)
                return Response({
                        "message": "No se ha asignado el rol al usuario satisfactoriamente"
                    }, status=status.HTTP_400_BAD_REQUEST)
            return Response({
                "message": "Fallo en la creacion del Usuario"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(('GET',))
def UserAddView2(request, *args, **kwargs):
    def dictfetchall(cursor):
        columns = [col[0].lower() for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    if request.method == 'GET':
        with connections['usuarios_sci'].cursor() as cursor:
            query = "SELECT DISTINCT id as id_usuario, username_email, nombre, apellido, activo from usuarios where username_email != '@' and id not in(SELECT DISTINCT id from datos_usuarios where id_proyecto = "
            query += str(kwargs['id_proyecto'])
            query += ') order by nombre, apellido'
            # print(query)
            cursor.execute(query)
            response = dictfetchall(cursor)
            serializer = DatosUsuariosSerializer(
                response, many=True, partial=True)
            # print(serializer.data)
        return Response(serializer.data)

@api_view(('POST',))
def UserAddView(request, *args, **kwargs):
    try:
        if request.method == 'POST':
            serializer2 = UrolesUsuariosSerializer1(data={
                'id_rol': int(request.data['id_rol']),
                'id_usuario': int(request.data['id_usuario'])
            })
            
            if serializer2.is_valid():
                serializer2.save()
                return Response({
                    "message": "Se ha creado el usuario satisfactoriamente"
                }, status=status.HTTP_201_CREATED)
            print(serializer2.errors)
            return Response({
                "message": "Fallo en la creacion del Usuario"
            }, status=status.HTTP_400_BAD_REQUEST)
            
    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
# @permission_classes((AllowAny,))
def usrUpdate(request, *args, **kwargs):
    try:
        if request.method == 'PUT':
            # print(request.data)
            usuario = Usuarios.objects.filter(
                id=int(request.data['id']))[0]
            # print(usuario)
            usuario2 = UrolesUsuarios1.objects.filter(
                id_usuario=int(request.data['id']), id_rol=int(request.data['rol_a']))[0]
            # print(usuario2)
            if not usuario and not usuario2:
                raise Exception('Usuario No encontrado')

            serializer = UsuariosSerializer(usuario, data={
                'username_email': request.data['username_email'],
                'llavepassword': request.data['llavepassword'],
                'nombre': request.data['nombre'],
                'apellido': request.data['apellido'],
                'activo': request.data['activo']
            },partial=True)

            serializer2 = UrolesUsuariosSerializer1(usuario2,data={
                'id_rol': int(request.data['rol']),
            },partial=True)

            if serializer.is_valid() and serializer2.is_valid():
                serializer.save()
                serializer2.save()
                return Response({
                    "message": "Se Actualizo el Usuario"
                }, status=status.HTTP_201_CREATED)
            else:
                # print(serializer2.errors)
                # print(serializer.errors)
                return Response({
                    "message": "Fallo en la Actualizacion"
                }, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        print(str(e))
        return Response({
            "message": str(e)
        }, status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT'])
# @permission_classes((AllowAny,))
def passReset(request, *args, **kwargs):
    try:
        if request.method == 'PUT':

            usuario = Usuarios.objects.get(
                id=request.data['id_usuario'])
            if not usuario:
                raise Exception('Usuario No encontrado')
            h = hashlib.new('sha1', request.data['password'].encode())
            serializer = UsuariosSerializer(
                usuario, data={'password': str(h.hexdigest())}, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Cambio Correcto de la contraseña"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la Actualizacion"
            }, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_404_BAD_REQUEST)


#########VISTAS DE ROLES##############


@api_view(['GET'])
# @permission_classes((AllowAny,))
def RolesAll(request, *args, **kwargs):
    if request.method == 'GET':
        roles = Uroles.objects.filter(
            id_proyecto=kwargs['id_proyecto']).order_by('descripcion')
        serializer = UrolesSerializer(roles, many=True)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def RolesOne(request, *args, **kwargs):
    if request.method == 'GET':
        rol = Uroles.objects.get(id=kwargs['id'])
        serializer = UrolesSerializer(rol)
        # print(serializer.data)
        return Response(serializer.data)


@api_view(('POST',))
def RolesCreate(request):
    try:
        if request.method == 'POST':
            ultimo = Uroles.objects.filter(
                id_proyecto=request.data['id_proyecto']).order_by('id').last()
            serializer = UrolesSerializer(data={
                'id': ultimo.id + 1,
                'id_proyecto': request.data['id_proyecto'],
                'descripcion': request.data['descripcion'],
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Se ha creado el Rol satisfactoriamente"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la creacion del Rol"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def RolesUpdate(request, *args, **kwargs):
    try:
        if request.method == 'PUT':
            rol = Uroles.objects.get(
                id=request.data['id'])
            if not rol:
                raise Exception('Usuario No encontrado')

            serializer = UrolesSerializer(rol, data={
                'descripcion': request.data['descripcion'],
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Se Actualizo el Rol"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la Actualizacion"
            }, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def PermisosAll(request, *args, **kwargs):
    if request.method == 'GET':
        roles = Uroles.objects.filter(
            id_proyecto=kwargs['id_proyecto']).values_list('id', flat=True)
        permisos = UpermisosRoles.objects.filter(
            id_rol__in=roles).values_list('id_permiso', flat=True)
        codigos = UcodigosLov.objects.filter(
            codigo__in=permisos).values_list('grupo', flat=True)
        codigosG = UcodigosLov.objects.filter(grupo__in=codigos)
        serializer = UcodigosLovSerializer(codigosG, many=True)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def PermisosRolAll(request, *args, **kwargs):
    if request.method == 'GET':
        rol = Uroles.objects.filter(id=kwargs['id_rol']).values_list(
            'descripcion', flat=True)
        roles = Uroles.objects.filter(
            id_proyecto=kwargs['id_proyecto']).values_list('id', flat=True)
        permisos = UpermisosRoles.objects.filter(
            id_rol__in=roles).values_list('id_permiso', flat=True)
        permisosR = UpermisosRoles.objects.filter(
            id_rol=kwargs['id_rol']).values_list('id_permiso', flat=True)
        codigos = UcodigosLov.objects.filter(
            codigo__in=permisos).values_list('grupo', flat=True)
        codigosG = UcodigosLov.objects.filter(
            grupo__in=codigos).exclude(codigo__in=permisosR)
        serializer = UcodigosLovSerializer(codigosG, many=True)
        return Response({'rol': rol,
                         'data': serializer.data})


@api_view(['GET'])
# @permission_classes((AllowAny,))
def PermisosRol(request, *args, **kwargs):
    if request.method == 'GET':
        rol = Uroles.objects.filter(id=kwargs['id_rol']).values_list(
            'descripcion', flat=True)
        permisos = UpermisosRoles.objects.filter(
            id_rol=kwargs['id_rol']).values_list('id_permiso', flat=True)
        codigos = UcodigosLov.objects.filter(codigo__in=permisos)
        serializer = UcodigosLovSerializer(codigos, many=True)
        return Response({'rol': rol,
                         'data': serializer.data})


@api_view(['POST'])
# @permission_classes((AllowAny,))
def PermisosRolAdd(request):
    try:
        if request.method == 'POST':
            with connections['usuarios_sci'].cursor() as cursor:
                query = "INSERT INTO UPERMISOS_ROLES (id_permiso,id_rol ) VALUES ('"
                query += request.data['id_permiso']
                query += "','"
                query += request.data['id_rol']
                query += "')"
                # print(query)
                cursor.execute(query)
                permisos = UpermisosRoles.objects.filter(
                    id_rol=request.data['id_rol'], id_permiso=request.data['id_permiso']).values('id_rol', 'id_permiso')
                # print(permisos[0]['id_rol'])
                if (str(permisos[0]['id_rol']) == str(request.data['id_rol']) and str(permisos[0]['id_permiso']) == str(request.data['id_permiso'])):
                    return Response({
                        "message": "Se asigno el permiso satisfactoriamente"
                    }, status=status.HTTP_201_CREATED)
                return Response({
                    "message": "Fallo en la creacion del Rol"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def PermisosOne(request, *args, **kwargs):
    if request.method == 'GET':
        codigos = UcodigosLov.objects.get(codigo=kwargs['id'])
        serializer = UcodigosLovSerializer(codigos)
        return Response(serializer.data)


@api_view(('POST',))
def PermisosCreate(request):
    try:
        if request.method == 'POST':

            roles = Uroles.objects.filter(
                id_proyecto=request.data['id_proyecto']).values_list('id', flat=True)
            permisos = UpermisosRoles.objects.filter(id_rol__in=roles).values_list(
                'id_permiso', flat=True).order_by('id_permiso')
            codigos = UcodigosLov.objects.filter(
                codigo__in=permisos).values_list('grupo', flat=True)
            codigos = UcodigosLov.objects.filter(grupo__in=codigos).last()

            serializer = UcodigosLovSerializer(data={
                "grupo": codigos.grupo,
                "codigo": codigos.codigo + 1,
                "descripcion": request.data["descripcion"],
                "descripcion_grupo": codigos.descripcion_grupo,
                "descripcion_completa_cdg": "--permiso",
            }, partial=True)
            # print(serializer)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Se ha creado el Rol satisfactoriamente"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la creacion del Rol"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def PermisosUpdate(request, *args, **kwargs):
    try:
        if request.method == 'PUT':
            permiso = UcodigosLov.objects.get(
                codigo=request.data['codigo'], grupo=request.data['grupo'])
            if not permiso:
                raise Exception('Permiso No encontrado')

            serializer = UcodigosLovSerializer(permiso, data={
                "descripcion": request.data["descripcion"],
                "descripcion_completa_cdg": request.data["descripcion_completa_cdg"],
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Se Actualizo el Permiso"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la Actualizacion"
            }, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        print(e)
        return Response({
            "message": str(e)
        }, status=status.HTTP_404_NOT_FOUND)
