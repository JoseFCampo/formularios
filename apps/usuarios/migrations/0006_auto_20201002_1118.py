# Generated by Django 2.2 on 2020-10-02 16:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0005_auto_20200831_2341'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='vpermisosroles',
            table='V_PERMISOS_ROLES',
        ),
    ]
