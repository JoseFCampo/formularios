# Generated by Django 2.2 on 2020-10-08 18:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0006_auto_20201002_1118'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgvmUsuariosxProyecto',
            fields=[
                ('rowid', models.CharField(default=None, editable=False, max_length=100, primary_key=True, serialize=False)),
                ('id_usuario', models.IntegerField(blank=True, null=True)),
                ('id_proyecto', models.IntegerField(blank=True, null=True)),
                ('usuario', models.CharField(blank=True, max_length=100, null=True)),
                ('proyecto', models.CharField(blank=True, max_length=100, null=True)),
                ('titulo_proyecto', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'db_table': 'agvm_usuariosxproyecto',
                'managed': False,
            },
        ),
    ]
