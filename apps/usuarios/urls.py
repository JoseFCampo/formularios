
from django.urls import path, include
from django.conf.urls import url
from .views import (UserAddView2, UserRegisterView, LoginView, UserProjects, usersViewAll,UserAddView, userDetallesView,
                    usersViewOne,usrUpdate,passReset,RolesOne,RolesAll,RolesUpdate,RolesCreate,
                    PermisosAll,PermisosRol,PermisosOne,PermisosCreate,PermisosUpdate,PermisosRolAdd,PermisosRolAll)

urlpatterns = [
    ###### Vistas del usuario Registrado ############
    path('login/', LoginView, name='Login'),
    path('prj/', UserProjects, name='Proyectos'),
    path('du/<int:id_usuario>/', userDetallesView, name='uno'),
    ###### Vistas de los usuarios del proyecto ######
    path('a/<int:id_proyecto>/', usersViewAll, name='todo'),
    path('u/<int:id_usuario>/<int:id_proyecto>/', usersViewOne, name='uno'),
    path('create/', UserRegisterView, name='Registro'),
    path('update/',usrUpdate,name='Users Update'),
    path('add/<int:id_proyecto>/', UserAddView2, name='Agregar'),
    path('add/', UserAddView, name='Agregar'),
    path('passwordReset/',passReset, name='Password Reset'),
    ####### VISTAS DE LOS ROLES #####################
    path('roles/<int:id_proyecto>/', RolesAll, name='todo'),
    path('roles/u/<int:id>/', RolesOne, name='Uno'),
    path('roles/create/', RolesCreate, name='create'),
    path('roles/update/', RolesUpdate, name='update'),
    
    ###### Vistas Permisos #########################
    path('permisosrol/<int:id_rol>/', PermisosRol, name='por rol'),
    path('permisos/<int:id_proyecto>/', PermisosAll, name='x'),
    path('permisos/<int:id_proyecto>/<int:id_rol>/', PermisosRolAll, name='x'),
    path('permisos/u/<int:id>/', PermisosOne, name='Uno'),
    path('permisos/create/', PermisosCreate, name='create'),
    path('permisos/update/', PermisosUpdate, name='update'),
    path('permisosrol/add/', PermisosRolAdd, name='update'),


]
