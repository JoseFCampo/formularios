from django.db import connection, connections, models
from rest_framework import serializers
from rest_framework import fields
from rest_framework.validators import UniqueTogetherValidator
# from rest_framework_jwt.settings import api_settings
from .models import UcodigosLov, UrolesUsuarios1, Usuarios, DatosUsuarios, UsuariosDetalles, Uroles, UpermisosRoles,UrolesUsuarios


class UsuariosSerializer(serializers.ModelSerializer):
   class Meta:
        model = Usuarios
        fields = (
            'id',
            'username_email',
            'password',
            'llavepassword',
            'nombre',
            'apellido',
            'activo',
            'fsistema'
            )
        

class Usuarios2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = ('id',
                    'username_email',
                    'nombre',
                    'apellido',
                    'activo',
                    'fsistema',
                    )
class UcodigosLovSerializer(serializers.ModelSerializer):
     class Meta:
        model=UcodigosLov
        fields = (
            "grupo",
            "codigo",
            "descripcion",
            "descripcion_grupo",
            "descripcion_completa_cdg",)


           
class UsuariosDetallesSerializer(serializers.ModelSerializer):
    codigo_atributo = UcodigosLovSerializer(many=False, read_only=True)
    id_usuario= Usuarios2Serializer(many=False, read_only=True)
    class Meta:
        model = UsuariosDetalles
        fields = (        
            'id_usuario',
            "codigo_atributo",
            "valor_asignado",
        )
         

class DatosUsuariosSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = DatosUsuarios
        fields =(
            'id_rol',
            'descripcion',
            'id_usuario',
            'username_email',
            'nombre',
            'apellido',
            'activo',
        )



class UpermisosRolesSerializer(serializers.ModelSerializer):
    class Meta:
            model = UpermisosRoles
            fields =('id_rol',
                     'id_permiso')
            

class UrolesSerializer(serializers.ModelSerializer):
     class Meta:
            model = Uroles
            fields ='__all__'


class UrolesUsuariosSerializer(serializers.ModelSerializer):
    class Meta:
            model = UrolesUsuarios
            fields ='__all__'
            
class UrolesUsuariosSerializer1(serializers.ModelSerializer):
    class Meta:

            validators = [
                UniqueTogetherValidator(
                    queryset=UrolesUsuarios1.objects.all(),
                    fields=['id_rol', 'id_usuario']
                )
            ]
            model = UrolesUsuarios1
            # fields =('id_rol',
            #          'id_usuario')
            exclude = ['rowid']
            
    def create(self, validated_data):
        # return UrolesUsuarios1.objects.create(**validated_data)
        with connections['usuarios_sci'].cursor() as cursor:
            cursor.execute("INSERT INTO uroles_usuarios (id_rol, id_usuario) VALUES ( %s, %s)", [validated_data['id_rol'], validated_data['id_usuario']])
            cursor.execute('SELECT  id_rol, id_usuario FROM uroles_usuarios WHERE id_rol=%s AND id_usuario=%s',[validated_data['id_rol'], validated_data['id_usuario']])
            row = cursor.fetchone()
            print(row)
        return row
    
    def update(self,instance, validated_data):        
        instance.id_rol=validated_data.get('id_rol',instance.id_rol)
        instance.id_usuario=validated_data.get('id_usuario',instance.id_usuario)
        instance.save()
        return instance


# class UcodigosLovSerializer(serializers.ModelSerializer):
#     class Meta:
#                 model = UcodigosLov
#                 fields ='__all__'


# class UserSerializerWithToken(serializers.ModelSerializer):

#     token = serializers.SerializerMethodField()
#     password = serializers.CharField(write_only=True)

#     def get_token(self, obj):
#         jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
#         jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

#         payload = jwt_payload_handler(obj)
#         token = jwt_encode_handler(payload)
#         return token

#     def create(self, validated_data):
#         password = validated_data.pop('password', None)
#         instance = self.Meta.model(**validated_data)
#         if password is not None:
#             instance.set_password(password)
#         instance.save()
#         return instance

#     class Meta:
#         model = Usuarios
#         fields = ('token', 'username', 'password')
