# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Cdircolector(models.Model):
    codigo_co = models.IntegerField(blank=True, null=True)
    id = models.IntegerField(blank=True, null=True)
    nombre_co = models.CharField(max_length=70, blank=True, null=True)
    apellidos_co = models.CharField(max_length=70, blank=True, null=True)
    nombre_apellido = models.CharField(max_length=141, blank=True, null=True)
    apellido_nombre = models.CharField(max_length=141, blank=True, null=True)
    correo_co = models.CharField(max_length=100)
    username_email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    activo = models.CharField(max_length=1)
    cvlac = models.CharField(max_length=100, blank=True, null=True)
    gradou_co = models.CharField(max_length=100, blank=True, null=True)
    instituto_co = models.CharField(max_length=100, blank=True, null=True)
    interes_co = models.CharField(max_length=100, blank=True, null=True)
    nacionalidad_co = models.CharField(max_length=100, blank=True, null=True)
    notas_co = models.CharField(max_length=100, blank=True, null=True)
    seccion_co = models.CharField(max_length=100, blank=True, null=True)
    sistema_co = models.CharField(max_length=100, blank=True, null=True)
    titulo_co = models.CharField(max_length=100, blank=True, null=True)
    vinculo_co = models.CharField(max_length=100, blank=True, null=True)
    telefono_co = models.CharField(max_length=100, blank=True, null=True)
    fax_co = models.CharField(max_length=2, blank=True, null=True)
    gradou_txt = models.CharField(max_length=100, blank=True, null=True)
    factualizo = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cdircolector'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class UcodigosLov(models.Model):
    grupo = models.IntegerField()
    codigo = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    descripcion_grupo = models.CharField(max_length=200, blank=True, null=True)
    descripcion_completa_cdg = models.CharField(
        max_length=200, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ucodigos_lov'


class Udirentidades(models.Model):
    codigo_in = models.CharField(primary_key=True, max_length=20)
    nombre_in = models.CharField(max_length=110)
    direccion_in = models.CharField(max_length=50, blank=True, null=True)
    ciudad_in = models.CharField(max_length=20, blank=True, null=True)
    dpto_in = models.CharField(max_length=20, blank=True, null=True)
    pais_in = models.CharField(max_length=20, blank=True, null=True)
    correoe_in = models.CharField(max_length=50, blank=True, null=True)
    website_in = models.CharField(max_length=60, blank=True, null=True)
    postal_in = models.CharField(max_length=8, blank=True, null=True)
    ap_aereo_in = models.CharField(max_length=10, blank=True, null=True)
    telefono_in = models.CharField(max_length=25, blank=True, null=True)
    fax_in = models.CharField(max_length=20, blank=True, null=True)
    titulod_in = models.CharField(max_length=40, blank=True, null=True)
    director_in = models.CharField(max_length=40, blank=True, null=True)
    oficina_in = models.CharField(max_length=40, blank=True, null=True)
    contactoname_in = models.CharField(max_length=40, blank=True, null=True)
    cargocontacto_in = models.CharField(max_length=50, blank=True, null=True)
    compartido_in = models.CharField(max_length=1, blank=True, null=True)
    sistema_in = models.CharField(max_length=10, blank=True, null=True)
    naturaleza_in = models.IntegerField(blank=True, null=True)
    seccional_in = models.CharField(max_length=20, blank=True, null=True)
    logo_entidad = models.BinaryField(blank=True, null=True)
    descripcion = models.CharField(max_length=500, blank=True, null=True)
    latitud = models.DecimalField(
        max_digits=10, decimal_places=7, blank=True, null=True)
    longitud = models.DecimalField(
        max_digits=10, decimal_places=7, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udirentidades'


class Uhistorial(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_tipo = models.IntegerField()
    id_usuario = models.IntegerField(blank=True, null=True)
    id_proyecto = models.IntegerField(blank=True, null=True)
    id_tematica = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    registros = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uhistorial'


class UhistorialTipo(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    descripcion = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'uhistorial_tipo'


class UobjetosPermisos(models.Model):
    objeto = models.CharField(max_length=1000, blank=True, null=True)
    permiso = models.IntegerField()
    alias_para_el_usuario = models.CharField(
        max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uobjetos_permisos'
        unique_together = (('permiso', 'objeto'),)


class UpermisosRoles(models.Model):
    id_permiso = models.IntegerField(primary_key=True)
    id_rol = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'upermisos_roles'
        unique_together = (('id_permiso', 'id_rol'),)


class Uroles(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_proyecto = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uroles'


class UrolesUsuarios(models.Model):
    id_rol = models.IntegerField(primary_key=True)
    id_usuario = models.ForeignKey(
        'Usuarios', models.DO_NOTHING, db_column='id_usuario')
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uroles_usuarios'
        unique_together = (('id_rol', 'id_usuario'),)


class Usuarios(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    username_email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    llavepassword = models.CharField(max_length=100, blank=True, null=True)
    nombre = models.CharField(max_length=70, blank=True, null=True)
    apellido = models.CharField(max_length=70)
    activo = models.CharField(max_length=1)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuarios'


class UsuariosDetalles(models.Model):
    id_usuario = models.ForeignKey(
        Usuarios, models.DO_NOTHING, db_column='id_usuario', primary_key=True)
    codigo_atributo = models.IntegerField()
    valor_asignado = models.CharField(max_length=100, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuarios_detalles'
        unique_together = (('id_usuario', 'codigo_atributo'),)
