
from __future__ import unicode_literals
from django.db import connections, models
from django.db.models import query


class VPermisosRoles(models.Model):
    rowid = models.CharField(
        max_length=100, primary_key=True, default=None, editable=False)
    id = models.IntegerField()
    username_email = models.CharField(max_length=100)
    rol_id = models.IntegerField()
    rol_descripcion = models.CharField(max_length=100, blank=True, null=True)
    permiso_id = models.IntegerField()
    permiso_descripcion = models.CharField(
        max_length=100, blank=True, null=True)
    grupo = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'V_PERMISOS_ROLES'


class Cdircolector(models.Model):
    codigo_co = models.IntegerField(blank=True)
    id = models.IntegerField(primary_key=True)
    nombre_co = models.CharField(max_length=280, blank=True, null=True)
    apellidos_co = models.CharField(max_length=280, blank=True, null=True)
    correo_co = models.CharField(max_length=400, blank=True, null=True)
    activo = models.CharField(max_length=4, blank=True, null=True)
    cvlac = models.CharField(max_length=400, blank=True, null=True)
    gradou_co = models.CharField(max_length=400, blank=True, null=True)
    instituto_co = models.CharField(max_length=400, blank=True, null=True)
    interes_co = models.CharField(max_length=400, blank=True, null=True)
    nacionalidad_co = models.CharField(max_length=400, blank=True, null=True)
    notas_co = models.CharField(max_length=400, blank=True, null=True)
    seccional_co = models.CharField(max_length=400, blank=True, null=True)
    seccion_co = models.CharField(max_length=400, blank=True, null=True)
    sistema_co = models.CharField(max_length=400, blank=True, null=True)
    titulo_co = models.CharField(max_length=400, blank=True, null=True)
    vinculo_co = models.CharField(max_length=400, blank=True, null=True)
    telefono_co = models.CharField(max_length=400, blank=True, null=True)
    fax_co = models.CharField(max_length=160, blank=True, null=True)
    cargo_co = models.CharField(max_length=400, blank=True, null=True)
    gradou_txt = models.CharField(max_length=400, blank=True, null=True)
    clave = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cdircolector'


class UcodigosLov(models.Model):
    grupo = models.IntegerField()
    codigo = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=400, blank=True, null=True)
    descripcion_grupo = models.CharField(max_length=800, blank=True, null=True)
    descripcion_completa_cdg = models.CharField(
        max_length=800, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ucodigos_lov'


class Udirentidades(models.Model):
    codigo_in = models.CharField(primary_key=True, max_length=80)
    nombre_in = models.CharField(max_length=440)
    direccion_in = models.CharField(max_length=200, blank=True, null=True)
    ciudad_in = models.CharField(max_length=80, blank=True, null=True)
    dpto_in = models.CharField(max_length=80, blank=True, null=True)
    pais_in = models.CharField(max_length=80, blank=True, null=True)
    correoe_in = models.CharField(max_length=200, blank=True, null=True)
    website_in = models.CharField(max_length=240, blank=True, null=True)
    postal_in = models.CharField(max_length=32, blank=True, null=True)
    ap_aereo_in = models.CharField(max_length=40, blank=True, null=True)
    telefono_in = models.CharField(max_length=100, blank=True, null=True)
    fax_in = models.CharField(max_length=80, blank=True, null=True)
    titulod_in = models.CharField(max_length=160, blank=True, null=True)
    director_in = models.CharField(max_length=160, blank=True, null=True)
    oficina_in = models.CharField(max_length=160, blank=True, null=True)
    contactoname_in = models.CharField(max_length=160, blank=True, null=True)
    cargocontacto_in = models.CharField(max_length=200, blank=True, null=True)
    compartido_in = models.CharField(max_length=4, blank=True, null=True)
    sistema_in = models.CharField(max_length=40, blank=True, null=True)
    naturaleza_in = models.IntegerField(blank=True, null=True)
    seccional_in = models.CharField(max_length=80, blank=True, null=True)
    logo_entidad = models.BinaryField(blank=True, null=True)
    descripcion = models.CharField(max_length=2000, blank=True, null=True)
    latitud = models.DecimalField(
        max_digits=10, decimal_places=7, blank=True, null=True)
    longitud = models.DecimalField(
        max_digits=10, decimal_places=7, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udirentidades'


class Uhistorial(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    id_tipo = models.IntegerField()
    id_usuario = models.IntegerField(blank=True, null=True)
    id_proyecto = models.IntegerField(blank=True, null=True)
    id_tematica = models.IntegerField(blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    registros = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uhistorial'


class UhistorialTipo(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    descripcion = models.CharField(max_length=1200)

    class Meta:
        managed = False
        db_table = 'uhistorial_tipo'


class UobjetosPermisos(models.Model):
    objeto = models.CharField(max_length=4000, blank=True, null=True)
    permiso = models.IntegerField()
    alias_para_el_usuario = models.CharField(
        max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uobjetos_permisos'
        unique_together = (('permiso', 'objeto'),)


class UpermisosRoles(models.Model):
    id_permiso = models.IntegerField()
    id_rol = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'upermisos_roles'
        unique_together = (('id_permiso', 'id_rol'),)


class Uroles(models.Model):
    id = models.IntegerField(primary_key=True)
    id_proyecto = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uroles'


class UrolesUsuarios1(models.Model):
    rowid = models.CharField(
        max_length=100, primary_key=True, editable=False)
    id_rol = models.IntegerField(blank=False, null=False)
    id_usuario = models.IntegerField(blank=False, null=False)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uroles_usuarios'
        unique_together = (('id_rol', 'id_usuario'),)
    

class UrolesUsuarios(models.Model):
    id_rol = models.IntegerField(primary_key=True)
    id_usuario = models.IntegerField(blank=False, null=False)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uroles_usuarios'
        unique_together = (('id_rol', 'id_usuario'),)

class Usuarios(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    username_email = models.CharField(max_length=400)
    password = models.CharField(max_length=400)
    llavepassword = models.CharField(max_length=400, blank=True, null=True)
    nombre = models.CharField(max_length=280, blank=True, null=True)
    apellido = models.CharField(max_length=280)
    activo = models.CharField(max_length=4)
    fsistema = models.CharField(max_length=280, blank=True, null=True)
    
    def is_active(self):
        if (self.activo.upper() == 'S'):
            return True
        return False
    
    def is_authenticated(self):
            return True
    class Meta:
        managed = False
        db_table = 'usuarios'


class UsuariosDetalles(models.Model):
    rowid = models.CharField(max_length=100, primary_key=True, default=None, editable=False)
    id_usuario = models.ForeignKey(Usuarios, related_name='id_usuario', on_delete=models.CASCADE, db_column='id_usuario')
    codigo_atributo = models.ForeignKey(UcodigosLov, related_name='codigo_atributo', on_delete=models.CASCADE, db_column='codigo_atributo')
    valor_asignado = models.CharField(max_length=400, blank=True, null=True)
            
    class Meta:
        managed = False
        db_table = 'usuarios_detalles'
        unique_together = (('id_usuario', 'codigo_atributo'),)


class VHistorial(models.Model):
    id_funcionario = models.IntegerField(primary_key=True)  # AutoField?
    id_muestreo = models.IntegerField()
    metodologia = models.CharField(max_length=100, blank=True, null=True)
    id_proyecto = models.CharField(max_length=100, blank=True, null=True)
    nombre_estacion = models.CharField(max_length=100, blank=True, null=True)
    codigo_estacion = models.CharField(max_length=81, blank=True, null=True)
    id_estacion = models.IntegerField()
    codigo_estacion = models.CharField(max_length=81, blank=True, null=True)
    notas = models.CharField(max_length=1000, blank=True, null=True)
    fecha_carga = models.DateTimeField(blank=True, null=True)
    muestras = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'v_historial'


class DatosUsuarios(models.Model):
    rowid = models.CharField(max_length=100, primary_key=True, default=None, editable=False)
    id_rol = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=400, blank=True, null=True)
    id_usuario = models.IntegerField(blank=True, null=True, db_column='id')
    username_email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    llavepassword = models.CharField(max_length=100, blank=True, null=True)
    nombre = models.CharField(max_length=70, blank=True, null=True)
    apellido = models.CharField(max_length=70)
    codigo_atributo = models.IntegerField(blank=True, null=True)
    valor_asignado = models.CharField(max_length=400, blank=True, null=True) 
    activo = models.CharField(max_length=1)
    id_proyecto = models.IntegerField(blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'datos_usuarios'