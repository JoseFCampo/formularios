from django.db import connections
from .models import AgmUnidadesMedida
from django.db.models import Count, Q
from .serializers import AgmUnidadesMedidaSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view ,permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from datetime import datetime


@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmunidadesmedida_one(request, *args, **kwargs):
    if request.method == 'GET':
        agmunidadesmedida = AgmUnidadesMedida.objects.get(
            id_unidad_medida=kwargs['id_unidad_medida'])
        serializer = AgmUnidadesMedidaSerializer(agmunidadesmedida)
        return Response(serializer.data)
    
    
@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmunidadesmedida_all(request, *args, **kwargs):
    # print(request.headers)
    if request.method == 'GET':
        agmunidadesmedida = AgmUnidadesMedida.objects.all().order_by(
            '-id_unidad_medida')
        serializer = AgmUnidadesMedidaSerializer(agmunidadesmedida, many=True)
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def agmunidadesmedida_create(request):
    try:
        if request.method == 'POST':
            agmunidadesmedida = AgmUnidadesMedida.objects.filter(
                detalle=request.data['detalle'],)
            if agmunidadesmedida:
                raise Exception('Unidad ya creada en la tabla')

            ultimo = AgmUnidadesMedida.objects.all().order_by('id_unidad_medida').last()
            # print(ultimo.id_unidad_medida)
            serializer = AgmUnidadesMedidaSerializer(data={
                'id_unidad_medida':  ultimo.id_unidad_medida + 1,
                'detalle': request.data['detalle'],
                'descripcion': request.data['descripcion'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S')
            })

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "Se ha creado la Unidad satisfactoriamente"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": "Fallo en la creacion de la unidad"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@ api_view(['PUT'])
def agmunidadesmedida_update(request):
    try:
        if request.method == 'PUT':
            agmunidadesmedida = AgmUnidadesMedida.objects.filter(
                id_unidad_medida=request.data['id_unidad_medida'])
            if not agmunidadesmedida:
                raise Exception('Unidad No encontrada porfavor recargue')

            serializer = AgmUnidadesMedidaSerializer(
                agmunidadesmedida[0],
                data={
                    'id_unidad_medida': request.data['id_unidad_medida'], 'detalle': request.data['detalle'],
                    'descripcion': request.data['descripcion']
                },
                partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": "La Unidad ha sido actualizada"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": "No se ha actualizado la unidad, intentelo de nuevo"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
