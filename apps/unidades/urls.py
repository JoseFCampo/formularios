from apps.unidades.views import(
    agmunidadesmedida_one,
    agmunidadesmedida_create,
    agmunidadesmedida_update,
    agmunidadesmedida_all,)
from django.urls import path


urlpatterns = [
    path('u/<int:id_unidad_medida>/',
         agmunidadesmedida_one, name='uno'),
    path('', agmunidadesmedida_all, name='todo'),
    path('create/', agmunidadesmedida_create, name='create'),
    path('update/', agmunidadesmedida_update, name='update'),
]
