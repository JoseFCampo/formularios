from .models import AgmUnidadesMedida
from rest_framework import serializers
from rest_framework.validators import UniqueValidator


class AgmUnidadesMedidaSerializer(serializers.Serializer):
    id_unidad_medida = serializers.FloatField(
        validators=[UniqueValidator(queryset=AgmUnidadesMedida.objects.all())])
    detalle = serializers.CharField(
        max_length=20,
        trim_whitespace=True,
        validators=[UniqueValidator(queryset=AgmUnidadesMedida.objects.all())])
    descripcion = serializers.CharField(
        allow_blank=True, trim_whitespace=True, allow_null=True, max_length=100, required=False)
    fsistema = serializers.CharField(allow_null=True, max_length=100)

    def create(self, validated_data):
        return AgmUnidadesMedida.objects.create(**validated_data)

    def update(self, instance, validated_data):

        instance.id_unidad_medida = validated_data.get(
            'id_unidad_medida ', instance.id_unidad_medida)
        instance.detalle = validated_data.get('detalle', instance.detalle)
        instance.descripcion = validated_data.get(
            'descripcion', instance.descripcion)

        instance.save()
        return instance

    class Meta:
        model = AgmUnidadesMedida
