from django.db import models


class AgmUnidadesMedida(models.Model):
    id_unidad_medida = models.FloatField(primary_key=True)
    detalle = models.CharField(max_length=20, unique=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    fsistema = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agm_unidades_medida'
        verbose_name = 'Unidad de medida'
        verbose_name_plural = 'Unidades de medida'
