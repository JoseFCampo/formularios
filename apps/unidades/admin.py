from django.contrib import admin
from .models import AgmUnidadesMedida


class AgmUnidadesMedidaAdmin(admin.ModelAdmin):

    list_display = ('id_unidad_medida', 'detalle',
                    'descripcion')
    list_display_links = ('id_unidad_medida', 'detalle',
                          'descripcion')
    search_fields = ['id_unidad_medida', 'detalle',
                     'descripcion']


admin.site.register(AgmUnidadesMedida, AgmUnidadesMedidaAdmin)
