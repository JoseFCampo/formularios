from .views import *
from django.urls import path


urlpatterns = [
    path('', actualizacion, name='Actualizacion Quality Flag'),
    path('permisos', permisos, name='Permisos'),
]
