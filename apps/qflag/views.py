from django.db import connections
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime
from django.http import HttpResponse



def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@api_view(['POST'])
# @permission_classes((AllowAny,))
def actualizacion(request, *args, **kwargs):

    if request.method == 'POST':
        try:
            # print(request.data)
            for rd in request.data:
                # print(datetime.now().strftime('%d/%m/%Y'))
                query ="UPDATE AGD_QFLOG SET QF_NOTAS ='"
                query +=rd['QF_NOTAS']
                query +="' , MODO_ASIGNACION = 1, FMODIFICACION = '"
                query +=str(datetime.now().strftime('%d/%m/%Y'))
                query +="' WHERE ID_MUESTRA= "
                query +=str(rd['ID_MUESTRA'])
                query +=" AND QF_ASIGNAR = "
                query +=str(rd['QF_ASIGNAR'])
                
                # print(query)
                query1 ="UPDATE AGD_MUESTRAS_VARIABLES SET QUALITY_FLAG="
                query1 += str(rd['QF_ASIGNAR'])
                query1 +=" WHERE ID_MUESTRA="
                query1 += str(rd['ID_MUESTRA'])
                
                # print(query1)
                with connections['datos'].cursor() as cursor:
                    cursor.execute(query1)
                    cursor.execute(query)
            return Response({
                    "message": "Quality Flags Actualizadas"
                }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                "message": str(e)
            }, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((AllowAny,))
def permisos(request, *args, **kwargs):
    # print(request.data)
    if request.method == 'POST':
        try:
            query="SELECT DISTINCT MVAR.ID_MUESTRA, MST.ID_MUESTREO, MTOS.ID_PROYECTO FROM AGD_MUESTRAS_VARIABLES MVAR, AGD_MUESTRAS MST, AGD_MUESTREOS MTOS WHERE MVAR.ID_MUESTRA = MST.ID_MUESTRA AND MST.ID_MUESTREO = MTOS.ID_MUESTREO AND MVAR.ID_MUESTRA IN ("
            query+=request.data+ ")"
            # print(query)
            with connections['datos'].cursor() as cursor:
                cursor.execute(query)
                response= dictfetchall(cursor)
            data={}
            # print(response)
            for r in response:
                data[r["ID_MUESTRA"]]=r["ID_PROYECTO"]
            return Response(data
                , status=status.HTTP_200_OK)
        except Exception as e:
            return Response({
                "message": str(e),
            }, status=status.HTTP_400_BAD_REQUEST)

# {"access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NjM1NiwiZXhwIjoxNjA2NDA2NjEwLCJpYXQiOjE2MDYyMzM4MTB9.O7Q5FKy5X_KYl5nhq6dTqbTD7RKI8rAAHu0M2ntdb_w",
# "userInfo":{
#     "id":6356,
#     "nombre":"Jose",
#     "username_email":"francisco.campo@invemar.org.co",
#     "roles":[
#         {"id":4,"descripcion":"prueba","id_proyecto":4},
#         {"id":3,"descripcion":"Otro","id_proyecto":4},
#         {"id":2,"descripcion":"Administrador","id_proyecto":1050},
#         {"id":1,"descripcion":"Administrador","id_proyecto":1511},
#         {"id":15,"descripcion":"Administrador","id_proyecto":1513},
#         {"id":96,"descripcion":"AdministradorAHN1","id_proyecto":2002},
#         {"id":17,"descripcion":"Administrador SIBM","id_proyecto":2004},
#         {"id":92,"descripcion":"Superadmin","id_proyecto":2027},
#         {"id":94,"descripcion":"Administrador","id_proyecto":2027},
#         {"id":176,"descripcion":"Administrador","id_proyecto":2050},
#         {"id":97,"descripcion":"Administrador_piangua","id_proyecto":2067},
#         {"id":100,"descripcion":"Administrador","id_proyecto":2069},
#         {"id":103,"descripcion":"Aministrador","id_proyecto":2079},
#         {"id":117,"descripcion":"Administrador","id_proyecto":2087},
#         {"id":140,"descripcion":"Administrador","id_proyecto":2148},
#         {"id":138,"descripcion":"Administrador","id_proyecto":2174},
#         {"id":145,"descripcion":"Administrador EPA","id_proyecto":2187},
#         {"id":999,"descripcion":"Todos","id_proyecto":2205},
#         {"id":158,"descripcion":"Administrador","id_proyecto":2239},
#         {"id":192,"descripcion":"Administrador cátalogo especies RBS","id_proyecto":2346},
#         {"id":181,"descripcion":"Administrador OFM","id_proyecto":2366},
#         {"id":190,"descripcion":"admin estaciones","id_proyecto":2507},
#         {"id":167,"descripcion":"Administrador","id_proyecto":3000},
#         {"id":151,"descripcion":"Administrador_GIRCC","id_proyecto":3002},
#         {"id":184,"descripcion":"administrador Conectividad","id_proyecto":3345},
#         {"id":180,"descripcion":"Administrador ARGOS","id_proyecto":3348},
#         {"id":187,"descripcion":"Administrador Bioprospeccion","id_proyecto":3356}],
#     "proyectos":[4,1050,1511,1513,2002,2004,2027,2050,2067,2069,2079,2087,2148,2174,2187,2205,2239,2346,2366,2507,3000,3002,3345,3348,3356]}}



""" 
[

{
    "QF_NOTAS":"xxx",
    "QF_ASIGNAR:"yyyy",
    "ID_MUESTRA":"ZZZZ",

},
{
    "QF_NOTAS":"xxx",
    "QF_ASIGNAR:"yyyy",
    "ID_MUESTRA":"ZZZZ",

},
{
    "QF_NOTAS":"xxx",
    "QF_ASIGNAR:"yyyy",
    "ID_MUESTRA":"ZZZZ",

},
{
    "QF_NOTAS":"xxx",
    "QF_ASIGNAR:"yyyy",
    "ID_MUESTRA":"ZZZZ",

},


]


 """