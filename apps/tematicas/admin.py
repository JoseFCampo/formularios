from django.contrib import admin
from .models import AgmTematicas


class AgmTematicasAdmin(admin.ModelAdmin):
    list_display = ('id_tematicas',
                    'raiz',
                    'descripcion',
                    'orden',
                    'es_ultimo',
                    'esquema',)
    list_display_links = ('id_tematicas',
                          'raiz',
                          'descripcion',
                          'orden',
                          'es_ultimo',
                          'esquema',)
    search_fields = ['id_tematicas',
                     'raiz',
                     'descripcion',
                     'orden',
                     'es_ultimo',
                     'esquema', ]


admin.site.register(AgmTematicas, AgmTematicasAdmin)
