from django.db import connections
from .models import AgmTematicas, VAgmTematicas
from django.db.models import Count, Q
from .serializers import AgmTematicasSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime

@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmtematicas_one(request, *args, **kwargs):
    if request.method == 'GET':
        agmtematicas = AgmTematicas.objects.get(
            id_tematicas=kwargs['id_tematicas'])
        serializer = AgmTematicasSerializer(agmtematicas)
        return Response(serializer.data)

@api_view(['GET'])
#@permission_classes((AllowAny,))
def agmtematicas_all(request, *args, **kwargs):
    if request.method == 'GET':
        agmtematicas = VAgmTematicas.objects.all().order_by(
             'descripcion','orden')
        serializer = AgmTematicasSerializer(agmtematicas, many=True, partial=True)
        return Response(serializer.data)
    
    
    
@api_view(['POST'])
def agmtematicas_create(request):
    try:
        if request.method == 'POST':

            agmtematicas = AgmTematicas.objects.filter(
                id_tematicass=request.data["id_tematicas"]
            )
            if agmtematicas:
                raise Exception('Tematica ya creada en la tabla')

            ultimo = AgmTematicas.objects.all().order_by('id_tematicas').last()
            serializer = AgmTematicasSerializer(data={
                'id_tematicas': ultimo.id_tematicas + 1,
                'raiz': request.data["raiz"],
                'descripcion': request.data["descripcion"],
                'orden': request.data["orden"],
                'esquema': request.data["esquema"],
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La tematica ha sido creada"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear la tematica"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def agmtematicas_update(request):
    try:
        if request.method == 'PUT':

            agmtematicas = AgmTematicas.objects.filter(
                id_tematicas=request.data['id_tematicas'])
            if not agmtematicas:
                raise Exception('not found')

            serializer = AgmTematicasSerializer(
                agmtematicas[0],
                data={
                    'id_tematicas': request.data['id_tematicas'],
                    'raiz': request.data["raiz"],
                    'descripcion': request.data["descripcion"],
                    'orden': request.data["orden"],
                    'esquema': request.data["esquema"],
                }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La Tematica ha sido actualizada"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar la tematica"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
