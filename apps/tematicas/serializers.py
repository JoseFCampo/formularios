from .models import AgmTematicas
from rest_framework import serializers
from django.db import connections


class AgmTematicasSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgmTematicas
        fields = (
            "id_tematicas",
            "raiz",
            "descripcion",
            "orden",
            "esquema"
        )
