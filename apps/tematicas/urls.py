from apps.tematicas.views import(
    agmtematicas_one,
    agmtematicas_create,
    agmtematicas_update,
    agmtematicas_all,)
from django.urls import path


urlpatterns = [
    path('u/<int:id_tematicas>/',
         agmtematicas_one, name='uno'),
    path('', agmtematicas_all, name='todo'),
    path('create/', agmtematicas_create, name='create'),
    path('update/', agmtematicas_update, name='update'),



]
