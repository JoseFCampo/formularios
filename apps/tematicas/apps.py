from django.apps import AppConfig


class TematicasConfig(AppConfig):
    name = 'tematicas'
