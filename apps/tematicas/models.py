from django.db import models


class AgmTematicas(models.Model):
    id_tematicas = models.FloatField(primary_key=True)
    raiz = models.FloatField(blank=True, null=True)
    descripcion = models.CharField(max_length=1000, blank=True, null=True)
    orden = models.FloatField(blank=True, null=True)
    es_ultimo = models.FloatField(blank=True, null=True)
    esquema = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agm_tematica'


class VAgmTematicas(models.Model):
    id_tematicas = models.FloatField(primary_key=True, db_column='CODIGO')
    descripcion = models.CharField(max_length=1000, blank=True, null=True, db_column='TEMATICA')
    orden = models.FloatField(blank=True, null=True)
    es_ultimo = models.FloatField(blank=True, null=True)
    esquema = models.CharField(max_length=50, blank=True, null=True, db_column='ESQUEMA_NOTAS')

    class Meta:
        managed = False
        db_table = 'v_agm_tematicas'
