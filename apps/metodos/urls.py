from apps.metodos.views import(
    agmmetodos_one,
    agmmetodos_create,
    agmmetodos_update,
    agmmetodos_all,)
from django.urls import path


urlpatterns = [
    path('u/<int:id_metodo>/',
         agmmetodos_one, name='uno'),
    path('', agmmetodos_all, name='todo'),
    path('create/', agmmetodos_create, name='create'),
    path('update/', agmmetodos_update, name='update'),



]
