from django.contrib import admin
from .models import AgmMetodos


class AgmMetodosAdmin(admin.ModelAdmin):
    list_display = ('id_metodo', 'nombre', 'cod_letras', 'descripcion')
    list_display_links = ('id_metodo', 'nombre', 'cod_letras', 'descripcion')
    search_fields = ['id_metodo', 'nombre', 'cod_letras', 'descripcion']


admin.site.register(AgmMetodos, AgmMetodosAdmin)
