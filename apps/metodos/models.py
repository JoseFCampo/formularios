from django.db import models

# Create your models here.


class AgmMetodos(models.Model):
    id_metodo = models.FloatField(primary_key=True)
    cod_letras = models.CharField(max_length=30, blank=True, null=True)
    nombre = models.CharField(max_length=100, blank=True, null=True)
    descripcion = models.CharField(max_length=1000, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agm_metodos'
