# Generated by Django 2.2 on 2020-06-23 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AgmMetodos',
            fields=[
                ('id_metodo', models.FloatField(primary_key=True, serialize=False)),
                ('cod_letras', models.CharField(blank=True, max_length=30, null=True)),
                ('nombre', models.CharField(blank=True, max_length=100, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=1000, null=True)),
                ('fsistema', models.DateField(blank=True, null=True)),
            ],
            options={
                'db_table': 'agm_metodos',
                'managed': False,
            },
        ),
    ]
