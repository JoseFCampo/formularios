from .models import AgmMetodos
from rest_framework import serializers
from django.db import connections


class AgmMetodosSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgmMetodos
        fields = ('id_metodo', 'nombre', 'cod_letras',
                  'descripcion','fsistema')
