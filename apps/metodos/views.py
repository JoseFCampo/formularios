from django.db import connections
from .models import AgmMetodos
from django.db.models import Count, Q
from .serializers import AgmMetodosSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime


@api_view(['GET'])
# @permission_classes((AllowAny,))
def agmmetodos_one(request, *args, **kwargs):
    if request.method == 'GET':
        agmmetodos = AgmMetodos.objects.get(
            id_metodo=kwargs['id_metodo'])
        serializer = AgmMetodosSerializer(agmmetodos)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def agmmetodos_all(request, *args, **kwargs):
    if request.method == 'GET':
        agmmetodos = AgmMetodos.objects.all().order_by(
            '-id_metodo')
        serializer = AgmMetodosSerializer(agmmetodos, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def agmmetodos_create(request):
    try:
        if request.method == 'POST':

            agmmetodos = AgmMetodos.objects.get(
                nombre=request.data['nombre'],
                cod_letras=request.data['cod_letras'])
            if agmmetodos:
                raise Exception('Parametro ya creado en la tabla')

            ultimo = AgmMetodos.objects.all().order_by('id_metodo').last()
            serializer = AgmMetodosSerializer(data={
                'id_metodo': ultimo.id_metodo + 1,
                'nombre': request.data['nombre'],
                'cod_letras': request.data['cod_letras'],
                'descripcion': request.data['descripcion'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El Metodo ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Error al crear el metodo",
                "error": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def agmmetodos_update(request):
    try:
        if request.method == 'PUT':

            agmmetodos = AgmMetodos.objects.filter(
                id_metodo=request.data['id_metodo'])
            if not agmmetodos:
                raise Exception('not found')
            serializer = AgmMetodosSerializer(
                agmmetodos[0],
                data={
                    'id_metodo': request.data['id_metodo'],
                    'nombre': request.data['nombre'],
                    'cod_letras': request.data['cod_letras'],
                    'descripcion': request.data['descripcion']
                }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El Metodo ha sido actualizado"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Error al actualizar el metodo"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
