from apps.referentes.views import(
    aglov_list,
    aglov_create,
    aglov_update,
    aglov_all,
    aglov_last,
    aglov_pag,
    aglov_ref
)
from django.urls import path

urlpatterns = [
    path('<int:tabla>/', aglov_list, name='tabla'),
    path('ref/<int:tab>,<int:cod_numerico>/', aglov_ref, name='refe'),
    path('create/', aglov_create, name='create'),
    path('update/', aglov_update, name='update'),
    path('page/<int:page>/<int:limit>/<str:search>/', aglov_pag, name='pagina'),
    path('', aglov_all, name='todo'),
    path('last/', aglov_last, name='ultimo'),

]
