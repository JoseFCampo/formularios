from django.apps import AppConfig


class ReferentesConfig(AppConfig):
    name = 'referentes'
