from django.db import models
from django.db import connections
from datetime import datetime


class AgLov(models.Model):
    rowid = models.CharField(
        max_length=100, primary_key=True, default=None, editable=False)
    tabla = models.IntegerField()
    cod_numerico = models.IntegerField()
    cod_alfabetico = models.CharField(max_length=300, blank=True, null=True)
    descripcion = models.CharField(max_length=500, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    descripcion_tabla = models.CharField(max_length=100, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True,)

    def __str__(self):
        return '%s %s' % (self.descripcion_tabla, self.descripcion)

    class Meta:
        managed = False
        db_table = 'ag_lov'
        verbose_name = 'Referente'
        verbose_name_plural = 'Referentes'

        unique_together = (('tabla', 'cod_numerico'),)

    def save(self, *args, **kwargs):
        self.fsistema = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        with connections['datos'].cursor() as cursor:
            cursor.execute("INSERT INTO AG_LOV (tabla, cod_numerico, cod_alfabetico, descripcion, orden, descripcion_tabla,fsistema) VALUES ( %s, %s, %s, %s, %s, %s, %s)", [
                           self.tabla, self.cod_numerico, self.cod_alfabetico, self.descripcion, self.orden, self.descripcion_tabla, self.fsistema])
            cursor.execute('SELECT * FROM AG_LOV WHERE tabla=%s AND cod_numerico=%s',
                           [self.tabla, self.cod_numerico])
            row = cursor.fetchone()
            # print(row)
        return row
