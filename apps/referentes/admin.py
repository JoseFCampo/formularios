from django.contrib import admin

from apps.referentes.models import AgLov
# Register your models here.


class AgLovAdmin(admin.ModelAdmin):
    def delete(self, obj):
        return '<input type="button" value="Borrar" onclick="location.href=\'{0}/delete/\'" />'.format(obj.pk)

    list_display = ('tabla', 'descripcion_tabla', 'descripcion', 'cod_numerico',
                    'cod_alfabetico', 'orden', 'fsistema')
    list_display_links = ('tabla', 'descripcion_tabla', 'descripcion')
    search_fields = ['descripcion_tabla', 'descripcion']
    fieldsets = (
        (None, {
            'fields': (('tabla', 'descripcion_tabla'), 'descripcion', ('cod_numerico', 'cod_alfabetico'), 'orden')
        }),
    )
    list_filter = ['descripcion_tabla']


admin.site.register(AgLov, AgLovAdmin)
