# Generated by Django 3.0.6 on 2020-05-19 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AgLov',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tabla', models.FloatField()),
                ('cod_numerico', models.FloatField()),
                ('cod_alfabetico', models.CharField(blank=True, max_length=30, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=100, null=True)),
                ('orden', models.FloatField(blank=True, null=True)),
                ('descripcion_tabla', models.CharField(blank=True, max_length=30, null=True)),
            ],
            options={
                'db_table': 'ag_lov',
                'managed': False,
            },
        ),
    ]
