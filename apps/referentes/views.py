from django.db import connections
from .models import AgLov
from django.db.models import Count, Q
from .serializers import AgLovSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime

@api_view(['GET'])
#@permission_classes((AllowAny,))
def aglov_list(request, *args, **kwargs):
    if request.method == 'GET':
        aglov = AgLov.objects.filter(tabla=kwargs['tabla'])
        serializer = AgLovSerializer(aglov, many=True)
        return Response(serializer.data)

@api_view(['GET'])
#@permission_classes((AllowAny,))
def aglov_ref(request, *args, **kwargs):
    if request.method == 'GET':
        aglov = AgLov.objects.filter(
            tabla=kwargs['tab'], cod_numerico=kwargs['cod_numerico'])[0]
        serializer = AgLovSerializer(aglov)
        return Response(serializer.data)

@api_view(['GET'])
#@permission_classes((AllowAny,))
def aglov_pag(request, *args, **kwargs):
    if request.method == 'GET':
        queryFilter = ""
        page = kwargs['page']
        limit = kwargs['limit']
        skip = limit * (page - 1)

        if kwargs["search"] == "ALL":
            aglov = AgLov.objects.all().order_by(
                '-tabla', '-cod_numerico')[skip: limit * page]
            count = AgLov.objects.all().count()
        elif kwargs["search"]:
            queryFilter = kwargs["search"]
            count = AgLov.objects.filter(
                descripcion_tabla__icontains=queryFilter).count()
            aglov = AgLov.objects.filter(Q(descripcion_tabla__icontains=queryFilter) | Q(descripcion__icontains=queryFilter)).order_by(
                '-tabla', '-cod_numerico')[skip: limit * page]
        serializer = AgLovSerializer(aglov, many=True)
        return Response({'count': count, 'data': serializer.data})

@api_view(['GET'])
#@permission_classes((AllowAny,))
def aglov_all(request, *args, **kwargs):
    if request.method == 'GET':
        aglov = AgLov.objects.all().order_by(
            'tabla', 'cod_numerico', 'orden')
        serializer = AgLovSerializer(aglov, many=True)
        return Response(serializer.data)

@api_view(['GET'])
#@permission_classes((AllowAny,))
def aglov_last(request, *args, **kwargs):
    if request.method == 'GET':
        aglov = AgLov.objects.order_by('-tabla', '-cod_numerico')[0]
        serializer = AgLovSerializer(aglov)
        return Response(serializer.data)


@api_view(['POST'])
def aglov_create(request):
    try:
        if request.method == 'POST':
            aglov = AgLov.objects.filter(
                tabla=request.data['tabla'], cod_numerico=request.data['cod_numerico'])
            if aglov:
                raise Exception('Parametro creado en la tabla')

            serializer = AgLovSerializer(data={
                'cod_alfabetico': request.data['cod_alfabetico'],
                'descripcion': request.data['descripcion'],
                'orden': request.data['orden'],
                'descripcion_tabla': request.data['descripcion_tabla'],
                'tabla': request.data['tabla'],
                'cod_numerico': request.data['cod_numerico'],
                'fsistema': datetime.now().strftime(
                    '%Y/%m/%d %H:%M:%S'),
            }, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El referente ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Ha ocurrido un error al Crear"
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)


@ api_view(['PUT'])
def aglov_update(request):
    try:
        if request.method == 'PUT':

            aglov = AgLov.objects.filter(
                tabla=request.data['tabla'], cod_numerico=request.data['cod_numerico'])
            if not aglov:
                raise Exception('not found')

            serializer = AgLovSerializer(aglov[0], data={
                'cod_alfabetico': request.data['cod_alfabetico'],
                'descripcion': request.data['descripcion'],
                'orden': request.data['orden'],
                'descripcion_tabla': request.data['descripcion_tabla'],
                'tabla': request.data['tabla'],
                'cod_numerico': request.data['cod_numerico'],
            }, partial=True)

            if serializer.is_valid():
                serializer.update({
                    'cod_alfabetico': request.data['cod_alfabetico'],
                    'descripcion': request.data['descripcion'],
                    'orden': request.data['orden'],
                    'descripcion_tabla': request.data['descripcion_tabla'],
                    'tabla': request.data['tabla'],
                    'cod_numerico': request.data['cod_numerico'],
                })
                return Response({
                    "message": " El referente ha sido actualizado"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Ha ocurrido un error al actualizar"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)
