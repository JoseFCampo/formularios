from .models import AgLov
from rest_framework import serializers
from django.db import connections


class AgLovSerializer(serializers.ModelSerializer):

    class Meta:
        model = AgLov
        fields = ('tabla', 'cod_numerico', 'cod_alfabetico', 'descripcion',
                  'orden', 'descripcion_tabla', 'fsistema')
        # read_only_fields = ('id',)

    def create(self, validated_data):
        with connections['datos'].cursor() as cursor:
            cursor.execute("INSERT INTO AG_LOV (tabla, cod_numerico, cod_alfabetico, descripcion, orden, descripcion_tabla,fsistema) VALUES ( %s, %s, %s, %s, %s, %s, %s)", [
                           validated_data['tabla'], validated_data['cod_numerico'], validated_data['cod_alfabetico'], validated_data['descripcion'], validated_data['orden'], validated_data['descripcion_tabla'], validated_data['fsistema']])
            cursor.execute('SELECT * FROM AG_LOV WHERE tabla = %s AND cod_numerico=%s',
                           [validated_data['tabla'], validated_data['cod_numerico']])
            row = cursor.fetchone()
        return row

    def update(self, validated_data):
        with connections['datos'].cursor() as cursor:
            cursor.execute('UPDATE AG_LOV SET tabla = %s, cod_numerico = %s, cod_alfabetico = %s, descripcion = %s, orden = %s, descripcion_tabla = %s WHERE tabla = %s AND cod_numerico = %s', [
                validated_data['tabla'], validated_data['cod_numerico'], validated_data['cod_alfabetico'], validated_data['descripcion'], validated_data['orden'], validated_data['descripcion_tabla'], validated_data['tabla'], validated_data['cod_numerico']])

            cursor.execute('SELECT * FROM AG_LOV WHERE tabla=%s AND cod_numerico=%s',
                           [validated_data['tabla'], validated_data['cod_numerico']])
            row = cursor.fetchone()
        return row
