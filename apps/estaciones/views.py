from django.db import connections
from django.db.models.fields import NullBooleanField
from .models import *
from apps.proyectos.models import AgmMetodologiasxProyecto
from apps.metodologias.models import AgmMetodologias, AglMetodologiaEquivalente
from django.db.models import Count, Q
from .serializers import CestacionesCamposSerializer, CestacionesRaizSerializer, CestacionesAtributosSerializer, ClocalidadtSerializer, CpaisestSerializer, CodigosLovSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionescampos_one(request, *args, **kwargs):
    if request.method == 'GET':
        cestacionescampos = CestacionesCampos.objects.get(
            id_item=kwargs['id_item'])
    serializer = CestacionesCamposSerializer(cestacionescampos)
    return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionescampos_all(request, *args, **kwargs):
    if request.method == 'GET':
        cestacionescampos = CestacionesCampos.objects.all().order_by('id_item')
        serializer = CestacionesCamposSerializer(cestacionescampos, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def cestacionescampos_create(request):
    try:
        if request.method == 'POST':
            ultimo = CestacionesCampos.objects.all().order_by('id_item').last()

            dataser = {
                'id_item': ultimo.id_item + 1,
                'columna_encabezado': request.data['columna_encabezado'],
                'tipo_dato': request.data['tipo_dato'],
                'grupo': request.data['grupo'],
                'vigente': request.data['vigente'],
                'descripcion_item': request.data['descripcion_item'],
                'orden': request.data['orden'],

            }
            dataser['tabla'] = request.data['tabla'] if request.data['tabla'] != '' else None
            dataser['conjunto'] = request.data['conjunto'] if request.data['conjunto'] != '' else None
            serializer = CestacionesCamposSerializer(
                data=dataser, partial=True)

            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El campo ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear el campo",
                "error": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def cestacionescampos_update(request):
    try:
        if request.method == 'PUT':

            cestacionescampos = CestacionesCampos.objects.filter(
                id_item=request.data['id_item'])
            if not cestacionescampos:
                raise Exception('not found')

            serializer = CestacionesCamposSerializer(
                cestacionescampos[0],
                data={
                    'id_item': request.data['id_item'],
                    'columna_encabezado': request.data['columna_encabezado'],
                    'tipo_dato': request.data['tipo_dato'],
                    'tabla': request.data['tabla'],
                    'conjunto': request.data['conjunto'],
                    'vigente': request.data['vigente'],
                    'descripcion_item': request.data['descripcion_item'],
                    'orden': request.data['orden'],
                    'grupo': request.data['grupo']
                }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El campo ha sido actualizada",
                    "error": serializer.errors
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar crear el campo"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


#########################################################################

@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionesraiz_one(request, *args, **kwargs):
    if request.method == 'GET':
        cestacionesraiz = CestacionesRaiz.objects.get(
            id_estacion=kwargs['id_estacion'])
        serializer = CestacionesRaizSerializer(cestacionesraiz)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionesraiz_all(request, *args, **kwargs):
    if request.method == 'GET':
        if kwargs["id_proyecto"] == 3348:
            cestacionesraiz = CestacionesRaiz.objects.all().order_by('-id_estacion')
            serializer = CestacionesRaizSerializer(cestacionesraiz, many=True)
            return Response(serializer.data)
        else:
            cestacionesraiz = CestacionesRaiz.objects.filter(
                id_proyecto=kwargs["id_proyecto"]).order_by('-id_estacion')
            serializer = CestacionesRaizSerializer(cestacionesraiz, many=True)
            data=[]
            for dat in serializer.data:
                if dat["id_metodologia"]:
                    dat["metodologia"]= AgmMetodologias.objects.get(id_metodologia=dat["id_metodologia"]).nombre
                else: 
                    dat["metodologia"]= id_metodologia=dat["id_metodologia"]
                dat["tipo_nodo"]=CodigosLov.objects.get(grupo=2, codigo=dat["tipo_nodo"]).descripcion
                data.append(dat)
            return Response(serializer.data)


@api_view(['POST'])
def cestacionesraiz_create(request):
    try:
        if request.method == 'POST':
            # cestacionesraiz = CestacionesRaiz.objects.filter(
            #     id_estacions=request.data["id_estacion"]
            # )
            # if cestacionesraiz:
            #     raise Exception('Estacion ya creada en la tabla')

            ultimo = CestacionesRaiz.objects.all().order_by(
                '-id_estacion').first().id_estacion

            print(request.data)

            dataser = {
                'id_estacion': (ultimo + 1),
                'id_proyecto': request.data['id_proyecto'],
                'id_tipo_registro': request.data['id_tipo_registro'],
                'prefijo_estacion': request.data['prefijo_estacion'],
                'consecutivo_estacion': request.data['consecutivo_estacion'],
                'pais_loc': request.data['pais_loc'],
                'tipo_nodo': request.data['tipo_nodo'],
                'nodo_final': request.data['nodo_final'],
                'vigente': request.data['vigente'],
            }
            if request.data['nodo_padre'] is not None:
                dataser['nodo_padre'] = request.data['nodo_padre']

            if request.data['id_metodologia'] is not None:
                dataser['id_metodologia'] = int(request.data['id_metodologia'])

            serializer = CestacionesRaizSerializer(data=dataser, partial=True)

            print(serializer.initial_data)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La estacion Base ha sido creada, redirigiendo a campos Adicionales...",
                    "id_estacion": (ultimo + 1)
                }, status=status.HTTP_201_CREATED)
            print(serializer.errors)
            return Response({
                "message": " Fallo al crear la estacion",
                "error": serializer.errors
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def cestacionesraiz_update(request):
    try:
        if request.method == 'PUT':

            cestacionesraiz = CestacionesRaiz.objects.filter(
                id_estacion=request.data['id_estacion'])
            if not cestacionesraiz:
                raise Exception('not found')

            serializer = CestacionesRaizSerializer(
                cestacionesraiz[0],
                data={
                    'id_estacion': request.data['id_estacion'],
                    'id_proyecto': request.data['id_proyecto'],
                    'id_metodologia': request.data['id_metodologia'],
                    'id_tipo_registro': request.data['id_tipo_registro'],
                    'prefijo_estacion': request.data['prefijo_estacion'],
                    'consecutivo_estacion': request.data['consecutivo_estacion'],
                    'pais_loc': request.data['pais_loc'],
                    'tipo_nodo': request.data['tipo_nodo'],
                    'nodo_padre': request.data['nodo_padre'],
                    'nodo_final': request.data['nodo_final'],
                    'vigente': request.data['vigente'],
                }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " La estacion ha sido actualizada"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar la estacion",
                    "error": serializer.errors
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)

##################################################################


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionesatributos_one(request, *args, **kwargs):
    if request.method == 'GET':
        # print(kwargs)
        cestacionesatributos = CestacionesAtributos.objects.get(
            id_estacion=kwargs['id_estacion'], id_item=kwargs['id_item'])
        serializer = CestacionesAtributosSerializer(cestacionesatributos)
        data=serializer.data
        campo= CestacionesCampos.objects.get(id_item=serializer.data['id_item'])
        data['id_item']=str(campo)
        print(data)
        return Response(data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionesatributos_all(request, *args, **kwargs):
    if request.method == 'GET':
 
        cestacionesatributos = CestacionesAtributos.objects.filter(
            id_estacion=kwargs['id_estacion']).order_by('id_item')
        serializer = CestacionesAtributosSerializer(
            cestacionesatributos, many=True)
        data=[]
        for dat in serializer.data:
            campo= CestacionesCampos.objects.get(id_item=dat['id_item'])
            dat['id_item']=str(campo)
            data.append(dat)
        return Response(data)
    
@api_view(['GET'])
# @permission_classes((AllowAny,))
def cestacionesatributos_opc(request, *args, **kwargs):
    if request.method == 'GET':
        metodologiasProyecto = AgmMetodologiasxProyecto.objects.filter(
            id_proyecto=kwargs["id_proyecto"]).values_list('id_metodologia', flat=True)

        exist1 = AglMetodologiaEquivalente.objects.filter(
            Q(id_metodologia__in=metodologiasProyecto) | Q(metodologia_equi__in=metodologiasProyecto)).exists()
        if exist1:
            metodEquiva = AglMetodologiaEquivalente.objects.filter(Q(id_metodologia__in=metodologiasProyecto) | Q(
            metodologia_equi__in=metodologiasProyecto)).first().metodologia_equi   
            codigosLov = CodigosLov.objects.filter(grupo=3, codigo__gte=((metodEquiva)*1000), codigo__lte=(((metodEquiva)*1000)+999)).order_by('orden')
            codigosLovB = []
            
            for cod in codigosLov:
                cod.codigo = cod.codigo-(metodEquiva*1000)
                codigosLovB.append(cod.codigo)
                
            codigosLov1=CodigosLov.objects.filter(grupo=3, codigo__gte=999000).order_by('orden')
            # print(codigosLov1)
            for cod in codigosLov1: 
                cod.codigo = cod.codigo-(999*1000)
                codigosLovB.append(cod.codigo)
           
            serializer= CestacionesCamposSerializer(CestacionesCampos.objects.filter(id_item__in=codigosLovB).order_by("descripcion_item"), many=True)
            return Response(serializer.data)
        



@api_view(['POST'])
def cestacionesatributos_create(request):
    try:
        if request.method == 'POST':
            cestacionesatributos = CestacionesAtributos.objects.filter(
                id_item=request.data["id_item"], id_estacion=request.data['id_estacion']
            )
            if cestacionesatributos:
                raise Exception('Estacion ya creada en la tabla')


            print(request.data)
            serializer = CestacionesAtributosSerializer(data={
                "id_estacion": request.data["id_estacion"],
                "id_metodologia": request.data["id_metodologia"],
                "id_item": request.data["id_item"],
                "valor": str(request.data["valor"]),

            }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El atributo ha sido creado"
                }, status=status.HTTP_201_CREATED)
            return Response({
                "message": " Fallo al crear el atributo",
                
            }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@ api_view(['PUT'])
def cestacionesatributos_update(request):
    try:
        if request.method == 'PUT':

            cestacionesatributos = CestacionesAtributos.objects.filter(
                id_item=request.data['id_item'], id_estacion=request.data['id_estacion'])
            if not cestacionesatributos:
                raise Exception('not found')

            serializer = CestacionesAtributosSerializer(
                cestacionesatributos[0],
                data={
                    "id_estacion": request.data["id_estacion"],
                    "id_metodologia": request.data["id_metodologia"],
                    "id_item": request.data["id_item"],
                    "valor": request.data["valor"],
                }, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    "message": " El atributo ha sido actualizado"
                }, status=status.HTTP_200_OK)
            else:
                return Response({
                    "message": " Fallo al actualizar el atributo"
                }, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        return Response({
            "message": str(e)
        }, status=status.HTTP_409_CONFLICT)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def cpaises_all(request, *args, **kwargs):
    if request.method == 'GET':
        cpaises = Cpaisest.objects.order_by('nombre_pa')
        serializer = CpaisestSerializer(
            cpaises, many=True)
        return Response(serializer.data)


@api_view(['GET'])
# @permission_classes((AllowAny,))
def tipoNodo_all(request, *args, **kwargs):
    if request.method == 'GET':
        metodologiasProyecto = AgmMetodologiasxProyecto.objects.filter(
            id_proyecto=kwargs["id_proyecto"]).values_list('id_metodologia', flat=True)

        exist1 = AglMetodologiaEquivalente.objects.filter(
            Q(id_metodologia__in=metodologiasProyecto) | Q(metodologia_equi__in=metodologiasProyecto)).exists()
        if exist1:
            metodEquiva = AglMetodologiaEquivalente.objects.filter(Q(id_metodologia__in=metodologiasProyecto) | Q(
                metodologia_equi__in=metodologiasProyecto)).first().metodologia_equi
            codigosLov = CodigosLov.objects.filter(grupo=4, codigo__gte=(
                (metodEquiva)*1000), codigo__lte=(((metodEquiva)*1000)+999)).order_by('orden')
            codigosLovB = []

            for cod in codigosLov:
                cod.codigo = cod.codigo-(metodEquiva*1000)
                codigosLovB.append(cod)
            serializer = CodigosLovSerializer(
                codigosLov, many=True)
            return Response(serializer.data)
        else:
            serializer = CodigosLovSerializer(
                CodigosLov.objects.filter(grupo=2, codigo=40), many=True)
            return Response(serializer.data)


@api_view(['POST'])
# @permission_classes((AllowAny,))
def nodoPadre_all(request, *args, **kwargs):
    if request.method == 'POST':
        nodos_padre = []
        if request.data["tipoNodo"] != 50:
            x = Clocalidadt.objects.filter(
                proyecto_loc=request.data["proyecto"], tipo_nodo=request.data["tipoNodo"]).values('path_lugar',"path_codigo","lugar", "id_estacion").order_by('path_lugar')
            for n in x:
                if len(n['path_lugar'])>140:
                    lugar= n['path_lugar'].split("/")
                    codigo= n['path_codigo'].split("/")
                    n['path_lugar'] = "/".join(codigo[0:len(codigo)-2])+"/"+"/".join(lugar[len(lugar)-2:len(lugar)])
                nodos_padre.append(n)
                    
        else:
            x = Clocalidadt.objects.filter(
                proyecto_loc=request.data["proyecto"], tipo_nodo=request.data["tipoNodo"]).values('lugar', "id_estacion").order_by('lugar')
            for n in x:
                n["path_lugar"] = n["lugar"]
                nodos_padre.append(n)
        serializer = ClocalidadtSerializer(nodos_padre, many=True)
        return Response(serializer.data)
