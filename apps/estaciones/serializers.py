from .models import *
from rest_framework import serializers
from django.db import connections


class ClstProyectosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClstProyectos
        fields = ('codigo', 'titulo', 'nombre_alterno', 'finicio', 'ffinalizo')


class ClocalidadtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clocalidadt
        fields = '__all__'


class CestacionesRaizSerializer(serializers.ModelSerializer):
    class Meta:
        model = CestacionesRaiz
        fields = (
            "id_estacion",
            "id_proyecto",
            "id_metodologia",
            "id_tipo_registro",
            "prefijo_estacion",
            "consecutivo_estacion",
            "pais_loc",
            "tipo_nodo",
            "nodo_padre",
            "nodo_final",
            "vigente",
        )


class CestacionesCamposSerializer(serializers.ModelSerializer):
    class Meta:
        model = CestacionesCampos
        fields = ("id_item",
                  "columna_encabezado",
                  "tipo_dato",
                  "tabla",
                  "conjunto",
                  "vigente",
                  "descripcion_item",
                  "orden",
                  "grupo",
                  "fsistema",
                  )


class CestacionesCampos1Serializer(serializers.ModelSerializer):
    class Meta:
        model = CestacionesCampos
        fields = ("id_item",
                  "columna_encabezado",
                  )


class CestacionesAtributosSerializer(serializers.ModelSerializer):
    class Meta:
        model = CestacionesAtributos
        fields = (
            "id_estacion",
            "id_metodologia",
            "id_item",
            "valor",
            "fsistema",
        )

    def create(self, validated_data):
        with connections['geograficos'].cursor() as cursor:
            cursor.execute("INSERT INTO CESTACIONES_ATRIBUTOS (id_estacion, id_item, id_metodologia, valor) VALUES ( %s, %s, %s, %s)", [
                int(validated_data['id_estacion'].id_estacion),int(validated_data['id_item'].id_item), int(validated_data['id_metodologia']), validated_data['valor']])
            cursor.execute('SELECT * FROM CESTACIONES_ATRIBUTOS WHERE id_estacion=%s AND id_item=%s',
                           [int(validated_data['id_estacion'].id_estacion), int(validated_data['id_item'].id_item)])
            row = cursor.fetchone()
        return row

    def update(self, validated_data):
        with connections['geograficos'].cursor() as cursor:
            cursor.execute('UPDATE CESTACIONES_ATRIBUTOS SET id_estacion = %i, id_item = %i, id_metodologia = %i, valor = %s  WHERE id_estacion = %i AND id_item = %s', [
             int(validated_data['id_estacion'].id_estacion), int(validated_data['id_item'].id_item),  int(validated_data['id_metodologia']), validated_data['valor'],   int(validated_data['id_estacion']),  int(validated_data['id_item'].id_item)])

            cursor.execute('SELECT * FROM CESTACIONES_ATRIBUTOS WHERE id_estacion=%i AND id_item=%i',
                           [ int(validated_data['id_estacion']),  int(validated_data['id_item'].id_item)])
            row = cursor.fetchone()
        return row


class CpaisestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cpaisest
        fields = ("codigo_pa",
                  "nombre_pa",
                  )


class CodigosLovSerializer(serializers.ModelSerializer):
    class Meta:
        model = CodigosLov
        fields = ("grupo",
                  "codigo",
                  "descripcion",
                  "sigla",
                  "documentacion",
                  "orden",
                  )
