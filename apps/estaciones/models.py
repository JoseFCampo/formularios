from django.db import models


class CestacionesRaiz(models.Model):
    id_estacion = models.IntegerField(primary_key=True)
    id_metodologia = models.IntegerField(blank=True, null=True)
    id_proyecto = models.ForeignKey(
        'ClstProyectos', models.DO_NOTHING, db_column='id_proyecto')
    id_tipo_registro = models.BooleanField(blank=True, null=True)
    prefijo_estacion = models.CharField(max_length=40, blank=True, null=True)
    consecutivo_estacion = models.IntegerField(blank=True, null=True)
    pais_loc =models.CharField(max_length=3, blank=True, null=True)
    nodo_padre = models.IntegerField(blank=True, null=True)
    nodo_final = models.BooleanField(blank=True, null=True)
    tipo_nodo = models.IntegerField()
    vigente = models.CharField(max_length=1, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cestaciones_raiz'


class CestacionesAtributos(models.Model):

    rowid = models.CharField(
        max_length=50, primary_key=True, default=None, editable=False)
    id_estacion = models.ForeignKey(
        'CestacionesRaiz', models.DO_NOTHING, db_column='id_estacion', blank=True, null=True)
    id_metodologia = models.IntegerField(blank=True, null=True)
    id_item = models.ForeignKey(
        'CestacionesCampos', models.DO_NOTHING, db_column='id_item', blank=True, null=True)
    valor = models.TextField()
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cestaciones_atributos'
        unique_together = (('id_estacion', 'id_metodologia', 'id_item'),)


class CestacionesCampos(models.Model):
    id_item = models.IntegerField(primary_key=True)
    columna_encabezado = models.CharField(max_length=25, blank=True, null=True)
    tipo_dato = models.IntegerField(blank=True, null=True)
    tabla = models.CharField(max_length=25, blank=True, null=True)
    conjunto = models.IntegerField(blank=True, null=True)
    vigente = models.BooleanField(blank=True, null=True)
    descripcion_item = models.CharField(max_length=50, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    grupo = models.IntegerField(blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    def __str__(self):
        return '%i. %s' % (self.id_item, self.columna_encabezado)

    class Meta:
        managed = False
        db_table = 'cestaciones_campos'


class ClstProyectos(models.Model):
    codigo = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=350)
    nombre_alterno = models.CharField(max_length=100)
    finicio = models.DateField(blank=True, null=True)
    ffinalizo = models.DateField(blank=True, null=True)
    ejecutor = models.CharField(max_length=30, blank=True, null=True)
    id_metadato_pro = models.IntegerField(blank=True, null=True)
    sistema = models.IntegerField(blank=True, null=True)
    logo_pro = models.BinaryField(blank=True, null=True)
    css_pro = models.CharField(max_length=15, blank=True, null=True)
    resumen = models.CharField(max_length=600, blank=True, null=True)
    fecha_sistema = models.DateField(blank=True, null=True)
    fcustodia_labsis = models.DateField(blank=True, null=True)
    codigo_char = models.CharField(unique=True, max_length=15)
    url_imagen = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clst_proyectos'


class Clocalidadt(models.Model):
    id_estacion = models.IntegerField(blank=True, null=True)
    secuencia_loc = models.IntegerField(blank=True, null=True)
    pais_loc = models.CharField(max_length=3, blank=True, null=True)
    proyecto_loc = models.FloatField(blank=True, null=True)
    nombre_alterno = models.CharField(max_length=100, blank=True, null=True)
    prefijo_cdg_est_loc = models.CharField(
        max_length=40, blank=True, null=True)
    codigo_estacion_loc = models.IntegerField(blank=True, null=True)
    path_codigo = models.CharField(max_length=4000, blank=True, null=True)
    path_lugar = models.CharField(max_length=4000, blank=True, null=True)
    vigente_loc = models.CharField(max_length=1, blank=True, null=True)
    id_metodologia = models.IntegerField(blank=True, null=True)
    id_tipo_registro = models.BooleanField(blank=True, null=True)
    nodo_padre = models.IntegerField(blank=True, null=True)
    nodo_final = models.BooleanField(blank=True, null=True)
    tipo_nodo = models.IntegerField(blank=True, null=True)
    tipo_nodo_des = models.CharField(max_length=120, blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)
    activo = models.CharField(max_length=4000, blank=True, null=True)
    ambiente_loc = models.FloatField(blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    area_ha = models.FloatField(blank=True, null=True)
    area_protegida = models.CharField(max_length=4000, blank=True, null=True)
    azimut = models.FloatField(blank=True, null=True)
    barco_loc = models.CharField(max_length=4000, blank=True, null=True)
    campana_loc = models.CharField(max_length=4000, blank=True, null=True)
    car = models.CharField(max_length=4000, blank=True, null=True)
    categoria_uni_manejo = models.CharField(
        max_length=4000, blank=True, null=True)
    cdg_dpto = models.FloatField(blank=True, null=True)
    cuenca_hdg = models.CharField(max_length=4000, blank=True, null=True)
    cuerpo_agua_loc = models.CharField(max_length=4000, blank=True, null=True)
    dato_temporal = models.CharField(max_length=4000, blank=True, null=True)
    desc_area = models.CharField(max_length=4000, blank=True, null=True)
    descripcion_estacion_loc = models.CharField(
        max_length=4000, blank=True, null=True)
    descripcion_nodo = models.CharField(max_length=4000, blank=True, null=True)
    distanciacosta_loc = models.FloatField(blank=True, null=True)
    dummy = models.FloatField(blank=True, null=True)
    ecorregion_loc = models.FloatField(blank=True, null=True)
    entidad_loc = models.CharField(max_length=4000, blank=True, null=True)
    error_d = models.CharField(max_length=4000, blank=True, null=True)
    fecha_instalacion_par = models.CharField(
        max_length=4000, blank=True, null=True)
    fechacreacion = models.CharField(max_length=4000, blank=True, null=True)
    fecha_sistema = models.DateField(blank=True, null=True)
    forma_parcela = models.FloatField(blank=True, null=True)
    geomorfologia = models.CharField(max_length=4000, blank=True, null=True)
    hondoagua_loc = models.FloatField(blank=True, null=True)
    latitudfin_loc = models.FloatField(blank=True, null=True)
    latitudinicio_loc = models.FloatField(blank=True, null=True)
    longitudfin_loc = models.FloatField(blank=True, null=True)
    longitudinicio_loc = models.FloatField(blank=True, null=True)
    lugar = models.CharField(max_length=4000, blank=True, null=True)
    marrio_loc = models.CharField(max_length=4000, blank=True, null=True)
    notas_loc = models.CharField(max_length=4000, blank=True, null=True)
    observaciones_loc = models.CharField(
        max_length=4000, blank=True, null=True)
    prof_max_loc = models.FloatField(blank=True, null=True)
    prof_min_loc = models.FloatField(blank=True, null=True)
    proyecto_dueno = models.CharField(max_length=4000, blank=True, null=True)
    region = models.CharField(max_length=4000, blank=True, null=True)
    salodulce_loc = models.CharField(max_length=4000, blank=True, null=True)
    secuencia_char = models.CharField(max_length=40, blank=True, null=True)
    sustrato_loc = models.FloatField(blank=True, null=True)
    tipo_componente = models.FloatField(blank=True, null=True)
    tipo_fisiografico = models.FloatField(blank=True, null=True)
    toponimia_loc = models.FloatField(blank=True, null=True)
    uac = models.FloatField(blank=True, null=True)
    wkt_area_muestreo = models.CharField(
        max_length=4000, blank=True, null=True)
    zona = models.CharField(max_length=4000, blank=True, null=True)
    zona_protegida = models.CharField(max_length=4000, blank=True, null=True)
    tipocosta_loc = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clocalidadt'
        


class CodigosLov(models.Model):
    grupo = models.IntegerField(blank=True, null=True)
    codigo = models.IntegerField(blank=True, null=True,primary_key=True)
    descripcion = models.CharField(max_length=120, blank=True, null=True)
    sigla = models.CharField(max_length=18, blank=True, null=True)
    documentacion = models.CharField(max_length=200, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    fsistema = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'codigos_lov'

class Cpaisest(models.Model):
    codigo_pa = models.CharField(primary_key=True, max_length=3)
    nombre_pa = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'cpaisest'
