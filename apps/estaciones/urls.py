from .views import(
    cestacionescampos_one,
    cestacionescampos_create,
    cestacionescampos_update,
    cestacionescampos_all,
    cestacionesraiz_one,
    cestacionesraiz_create,
    cestacionesraiz_update,
    cestacionesraiz_all,
    cestacionesatributos_one,
    cestacionesatributos_create,
    cestacionesatributos_update,
    cestacionesatributos_all,
    cpaises_all,
    tipoNodo_all,
    nodoPadre_all,
    cestacionesatributos_opc
)
from django.urls import path


urlpatterns = [
    path('campos/u/<int:id_item>/',
         cestacionescampos_one, name='unoc'),
    path('campos/create/', cestacionescampos_create, name='createc'),
    path('campos/update/', cestacionescampos_update, name='updatec'),
    path('campos/', cestacionescampos_all, name='todoc'),

    path('raiz/u/<int:id_estacion>/',
         cestacionesraiz_one, name='unor'),
    path('raiz/create/', cestacionesraiz_create, name='creater'),
    path('raiz/update/', cestacionesraiz_update, name='updater'),
    path('raiz/<int:id_proyecto>', cestacionesraiz_all, name='todor'),

    path('atributos/u/<int:id_estacion>,<int:id_item>/',
         cestacionesatributos_one, name='unoa'),
    path('atributos/create/', cestacionesatributos_create, name='createa'),
    path('atributos/update/', cestacionesatributos_update, name='updatea'),
    path('atributos/<int:id_estacion>/',
         cestacionesatributos_all, name='todoa'),

    path('paises/', cpaises_all, name='paises'),

    path('tipoNodo/<int:id_proyecto>', tipoNodo_all, name='tipoNodo'),
    path('NodoPadre/', nodoPadre_all, name='NodoPadre'),
    path('atributosOpc/<int:id_proyecto>', cestacionesatributos_opc,
         name='Opciiones de los Atributos'),
]
