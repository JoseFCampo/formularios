

[![Invemar](http://www.invemar.org.co/logos/logoINVEMAR.png)](http://www.invemar.org.co)

# Prerequisites
- [virtualenv](https://virtualenv.pypa.io/en/latest/)

# Initialize the project
Create and activate a virtualenv:

```bash
virtualenv env --no-site-packages --distribute -p /usr/bin/python3.x
or
pyvenv-3.x env
```

```bash
source env/bin/activate
```
# Install dependencies:

```bash
pip install -r requirements.txt
```



# Migrate, create a superuser, and run the server:

```bash
python forms/manage.py migrate
python forms/manage.py createsuperuser
python forms/manage.py runserver
```

# Start project with custom settings

```bash
python manage.py runserver [USER_HOST]:8000 
```